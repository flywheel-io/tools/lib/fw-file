"""fw_file package metadata."""

from importlib.metadata import version

__version__ = version(__name__)
NAME = "fw_file"
