"""Tests for nifti module."""

import nibabel
import numpy as np
import pytest

from fw_file.nifti import Nifti


def create_nifti(version="1"):
    header_class = getattr(getattr(nibabel, f"nifti{version}"), f"Nifti{version}Header")
    image_class = getattr(getattr(nibabel, f"nifti{version}"), f"Nifti{version}Image")
    return image_class(np.eye(3), None, header=header_class())


@pytest.fixture
def nifti(tmp_path):
    nifti_raw = create_nifti(version="1")
    path = tmp_path / "test.nii.gz"
    nibabel.save(nifti_raw, path)
    return Nifti(path)


@pytest.mark.parametrize("version", ["1", "2"])
def test_nifti_init_from_file(version, tmp_path):
    nifti_raw = create_nifti(version=version)
    path = tmp_path / "test.nii.gz"
    nibabel.save(nifti_raw, path)
    nifti = Nifti(path)

    assert nifti.nifti.header == nifti_raw.header
    assert np.array_equal(nifti.get_fdata(), nifti_raw.get_fdata())


def test_nifti_init_invalid_image(tmp_path):
    path = tmp_path / "test.nii.gz"
    path.write_bytes(b"foo")
    with pytest.raises(ValueError):
        Nifti(path)


def test_nifti_save(nifti, tmp_path):
    path = tmp_path / "destination.nii.gz"
    nifti.save(path)
    assert path.exists()


def test_nifti_meta(nifti):
    assert nifti.get_meta() == {
        "file.name": "test.nii.gz",
        "file.type": "nifti",
    }


def test_nifti_get(nifti):
    assert nifti.get("magic") == b"n+1"


def test_nifti_set(nifti):
    nifti["magic"] = b"n+2"
    assert nifti.get("magic") == b"n+2"


def test_nifti_del(nifti):
    with pytest.raises(TypeError):
        del nifti["magic"]


def test_nifti_len(nifti):
    assert len(nifti) == 43


def test_nifti_iter(nifti):
    assert len(list(nifti)) == 43
