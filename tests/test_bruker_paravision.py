"""Tests for the Bruker ParaVision (sucject/acqp/method) format."""

import pytest

from fw_file.bruker import ParaVision

from .conftest import ASSETS_ROOT

PV_ROOT = ASSETS_ROOT / "paravision"


def test_get_field():
    pv_acq = ParaVision(PV_ROOT / "acqp")
    pv_method = ParaVision(PV_ROOT / "method")
    pv_subject = ParaVision(PV_ROOT / "subject")
    assert pv_acq.acq_protocol_name == "ms_fexi_3_delcompmix"
    assert pv_acq.acq_abs_time == "(1548755100, 299, 60)"
    assert pv_method.pvm_scantimestr == "0h0m50s0ms"
    assert pv_subject.subject_id == "ice water ph"
    assert pv_subject.subject_dbirth == "01 Feb 2019"
    assert pv_subject.subject_study_name == "FEXI EVALUATION"
    assert pv_subject.subject_abs_date == "(1548606562, 570, 60)"
    assert "subject_abs_date" not in pv_acq
    assert "subject_abs_date" in pv_subject
    assert "subject_abs_date" in list(pv_subject)


def test_read_only():
    pv = ParaVision(PV_ROOT / "acqp")
    with pytest.raises(TypeError):
        pv.acq_protocol_name = "foo"
    with pytest.raises(TypeError):
        del pv["acq_protocol_name"]
    with pytest.raises(TypeError):
        pv.save()


def test_meta():
    pv_acq = ParaVision(PV_ROOT / "acqp")
    pv_subject = ParaVision(PV_ROOT / "subject")
    assert pv_acq.get_meta() == {
        "acquisition.label": "ms_fexi_3_delcompmix",
        "acquisition.timestamp": "2019-01-29T10:45:00.000+01:00",
        "acquisition.timezone": "Europe/Budapest",
        "file.name": "acqp",
        "file.type": "ParaVision",
    }
    assert pv_subject.get_meta() == {
        "subject.label": "ice water ph",
        "subject.sex": "unknown",
        "session.uid": "2.16.756.5.5.100.8323328.131863.1548606562.3410",
        "session.label": "FEXI EVALUATION - 20190127172922",
        "session.timestamp": "2019-01-27T17:29:22.000+01:00",
        "session.timezone": "Europe/Budapest",
        "file.name": "subject",
        "file.type": "ParaVision",
    }


def test_dir():
    pv = ParaVision(PV_ROOT / "acqp")
    assert "acq_protocol_name" in dir(pv)
    assert "fields" in dir(pv)
    assert "filepath" in dir(pv)
    assert "version" in dir(pv)


def test_pv360():
    pv_subject = ParaVision(PV_ROOT / "subject_pv360")
    assert pv_subject.subject_remarks == "fw://npil/QA\nBruker QA Phantom"
    assert pv_subject.get_meta() == {
        "subject.label": "QA-50ml",
        "session.label": "FW-filename_test_11-15-21 - 20211115190006",
        "session.uid": "2.16.756.5.5.100.8323328.5157.1636999206.671",
        "session.timestamp": "2021-11-15T19:00:06.000+01:00",
        "session.timezone": "Europe/Budapest",
        "file.name": "subject_pv360",
        "file.type": "ParaVision",
    }
