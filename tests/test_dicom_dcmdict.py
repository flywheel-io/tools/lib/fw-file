"""Tests for the dicom.dictionaries module."""

import io

from pydicom.datadict import private_dictionaries

from fw_file.dicom import load_dcmdict

DCMDICT = """
# comment line
invalid line
(0019,"Foo Creator",10)	ST	FooTag	1	PrivateTag
(0019-o-001b,"Bar Creator",10)	ST	BarTag	1	PrivateTag
"""


def test_load_dcmdict():
    file = io.StringIO()
    file.write(DCMDICT)
    file.seek(0)

    load_dcmdict(file)

    assert private_dictionaries["Foo Creator"] == {
        "0019xx10": ("ST", "1", "FooTag", "")
    }
    assert private_dictionaries["Bar Creator"] == {
        "0019xx10": ("ST", "1", "BarTag", ""),
        "001Bxx10": ("ST", "1", "BarTag", ""),
    }
