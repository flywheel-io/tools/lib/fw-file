"""Tests for the GE PFile format."""

import io
from datetime import datetime, timezone

import pytest

from fw_file.dicom.testing import merge_dict
from fw_file.ge import VERSION_OFFSETS, PFile, pack

PFILE_DEFAULTS = {"logo": "GE_MED_NMR"}


def create_pfile(version=28.002, end_of_file=None, **pfile_dict):
    pfile_dict = merge_dict(pfile_dict, PFILE_DEFAULTS)
    file = io.BytesIO(bytearray(256 << 10))
    file.seek(0)
    offsets = VERSION_OFFSETS[version]
    file.write(pack(version, "f"))
    for field, value in pfile_dict.items():
        offset, fmt = offsets[field]
        file.seek(offset)
        file.write(pack(value, fmt))
    if end_of_file:
        file.seek(256 << 10)
        file.write(end_of_file)
    file.seek(0)
    return PFile(file)


def test_init():
    pfile = create_pfile(end_of_file=b"foo")
    assert pfile.version == 28.002


def test_init_w_invalid_version():
    with pytest.raises(ValueError):
        PFile(io.BytesIO(pack(1.001, "f")))


def test_init_w_invalid_logo():
    with pytest.raises(ValueError):
        create_pfile(logo="foo")


def test_meta():
    pfile = create_pfile(
        PatientID="PatientID",
        PatientName="doe^jane",
        StudyInstanceUID="1",
        SeriesInstanceUID="1.2",
        SeriesDescription="my series",
        AcquisitionNumber=3,
        # 1 second after epoch UTC
        AcquisitionDateTime=1,
    )
    pfile.get_meta()
    assert pfile.get_meta() == {
        "subject.label": "PatientID",
        "subject.firstname": "Jane",
        "subject.lastname": "Doe",
        "session.uid": "1",
        # 1 hour and 1 second after epoch in Europe/Budapest TZ
        "session.timestamp": "1970-01-01T01:00:01.000+01:00",
        "session.timezone": "Europe/Budapest",
        "acquisition.uid": "1.2_3",
        "acquisition.label": "my series",
        "acquisition.timestamp": "1970-01-01T01:00:01.000+01:00",
        "acquisition.timezone": "Europe/Budapest",
        "file.type": "pfile",
    }


def test_meta_other_tz(mocker):
    # Timezone is cached, so can't just set it in the environment
    tzlocal = mocker.patch("tzlocal.get_localzone")
    tzlocal.return_value = timezone.utc

    pfile = create_pfile(
        PatientID="PatientID",
        PatientName="doe^jane",
        StudyInstanceUID="1",
        SeriesInstanceUID="1.2",
        SeriesDescription="my series",
        AcquisitionNumber=3,
        # 1 second after epoch UTC
        AcquisitionDateTime=1,
    )
    pfile.get_meta()
    assert pfile.get_meta() == {
        "subject.label": "PatientID",
        "subject.firstname": "Jane",
        "subject.lastname": "Doe",
        "session.uid": "1",
        # 1 hour and 1 second after epoch in Europe/Budapest TZ
        "session.timestamp": "1970-01-01T00:00:01.000+00:00",
        "session.timezone": "UTC",
        "acquisition.uid": "1.2_3",
        "acquisition.label": "my series",
        "acquisition.timestamp": "1970-01-01T00:00:01.000+00:00",
        "acquisition.timezone": "UTC",
        "file.type": "pfile",
    }


def test_ge_physio_match():
    pfile = create_pfile(
        PulseSequenceName="Test",
        AcquisitionDateTime=1,
        AcquisitionDuration=10,
    )
    assert pfile.ge_physio_match == {
        "psn": "test",
        "end": datetime(1970, 1, 1, 0, 0, 11, tzinfo=timezone.utc),
    }


def test_meta_acquisition_timestamp():
    pfile = create_pfile(AcquisitionDate="12/31/99", AcquisitionTime="23:59")
    assert pfile.get_meta()["acquisition.timestamp"] == "1999-12-31T23:59:00.000+01:00"
    assert pfile.get_meta()["acquisition.timezone"] == "Europe/Budapest"


def test_save(tmp_path):
    pfile = create_pfile(
        AcquisitionNumber=1, StudyInstanceUID="1.2", PulseSequenceName="psd"
    )
    # delete fields
    del pfile["AcquisitionNumber"]
    del pfile["PulseSequenceName"]
    # update fields
    pfile.SeriesInstanceUID = "1.2.345"
    filepath = tmp_path / "pfile"
    pfile.save(filepath)
    pfile = PFile(filepath)
    assert pfile.AcquisitionNumber == 0
    assert pfile.StudyInstanceUID == "1.2"
    assert pfile.PulseSequenceName == ""
    assert pfile.SeriesInstanceUID == "1.2.345"


def test_save_raises_on_invalid_file():
    pfile = create_pfile()
    with pytest.raises(ValueError):
        pfile.save()


def test_iter():
    pfile = create_pfile(AcquisitionNumber=1, StudyInstanceUID="1.2")
    assert list(VERSION_OFFSETS[28.002].keys()) == list(pfile)


def test_dir():
    pfile = create_pfile(AcquisitionNumber=1, StudyInstanceUID="1.2")
    expected = set(VERSION_OFFSETS[28.002].keys())
    expected.update({"filepath", "version"})
    assert expected.issubset(dir(pfile))
