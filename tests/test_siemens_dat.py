"""Tests for the Siemens MR RAW (.dat) file format."""

import io

import pytest

from fw_file.siemens import DATFile

DAT_INPUT = b"""
\x80Config\x80<XProtocol>
{
<ParamMap.""> {<ParamMap."PARC">
  {
    <ParamMap."RECOMPOSE">
    {
      <ParamString."PatientID">{ "PatientID" }
      <ParamString."PatientBirthDay">{ "19991231" }
      <ParamLong."PatientSex">{ 3 }
      <ParamString."tPatientName">{ "Last^First" }
      <ParamLong."SubProtocolIndex">{ }
      <ParamString."PatientLOID">{ }
      <ParamString."StudyLOID">{ "1.3.12.2.1107.5.2.43.166173.30000021031208005898700000003" }
      <ParamString."SeriesLOID">{ "1.3.12.2.1107.5.2.43.166173.2021031211174062948901906.0.0.0" }
      <ParamLong."ProtocolChangeHistory">{ }
      <ParamString."tStudyDescription">{ "StudyDescription" }
    }
  }
  <ParamString."PrepareTimestamp">{ "2021-03-12 11:19:31" }
}
\x80Dicom\x80<XProtocol>
{
  <ParamMap.""> {<ParamMap."DICOM">
  {
    <ParamDouble."flUsedPatientWeight">  { <Precision> 6  30.000000  }
    <ParamDouble."flPatientAge">{ 10.000000 }
  }
}
\x80Meas\x80<XProtocol>
{
  <NAME> "ONLINE Pipe Configuration
  <ParamMap."">
    {
      <ParamArray."StudyComment">
      {
        <DefaultSize> 1
        <MaxSize> 50
        <Default> <ParamString."">{ }
        { "StudyComments" }
      }
    }
  }
}
\x80Phoenix\x80<XProtocol>
{
  <ID> 1000001
  ...
}
""".lstrip()


def test_siemens_mr_raw_dat_meta():
    dat = DATFile(io.BytesIO(DAT_INPUT))
    assert dat.get_meta() == {
        "subject.label": "PatientID",
        "subject.firstname": "First",
        "subject.lastname": "Last",
        "subject.sex": "other",
        "session.uid": "1.3.12.2.1107.5.2.43.166173.30000021031208005898700000003",
        "session.label": "StudyDescription",
        "session.age": 315576000,
        "session.weight": 30.0,
        "session.timestamp": "2021-03-12T11:17:40.000+01:00",
        "session.timezone": "Europe/Budapest",
        "acquisition.uid": "1.3.12.2.1107.5.2.43.166173.2021031211174062948901906.0.0.0",
        "acquisition.label": "2021-03-12T11-17-40",
        "acquisition.timestamp": "2021-03-12T11:17:40.000+01:00",
        "acquisition.timezone": "Europe/Budapest",
        "file.type": "raw/siemens",
    }


def test_dat_raises_on_invalid_file():
    with pytest.raises(ValueError):
        DATFile(io.BytesIO(b"foobar"))
