"""Tests for RDA files (Siemens MR spectroscopy)."""

import io

import pytest

from fw_file.siemens import RDAFile

RDA_INPUT = b"""
>>> Begin of header <<<
FieldA: A
FieldB[0]: B0
FieldB[1]: B1
FieldC[0,0]: C00
FieldD[1H]: D1H
FieldE: 1
FieldF: 1.1
PatientName: Patient^Name
PatientID: PatientID
PatientSex: O
PatientBirthDate: 20101010
StudyDate: 20210312
StudyTime: 111202.7
StudyDescription: StudyDescription
PatientAge: 010Y
PatientWeight: 30
SeriesDate: 20210312
SeriesTime: 112743.7
SeriesDescription: SeriesDescription
ProtocolName: ProtocolName
PatientPosition: HFS
SeriesNumber: 7
InstitutionName: Flywheel
StationName: Budapest
ModelName: MAGNETO
DeviceSerialNumber: 1337
SoftwareVersion[0]: syngo MR XA30
InstanceCreationTime:
InstanceCreationDate:
InstanceNumber: 1
InstanceComments: InstanceComments
AcquisitionNumber: 1
SequenceName: *SequenceName
SequenceDescription: SequenceDescription
>>> End of header <<<
SPECTRODATA
""".strip().replace(b"\n", b"\r\n")


def test_rda():
    rda = RDAFile(io.BytesIO(RDA_INPUT))

    assert rda.FieldA == "A"
    assert rda.FieldB[0] == "B0"
    assert rda.FieldB[1] == "B1"
    assert rda.FieldC[0, 0] == "C00"
    assert rda.FieldD["1H"] == "D1H"
    assert rda.FieldE == 1
    assert rda.FieldF == 1.1
    assert rda.get_meta() == {
        "subject.label": "PatientID",
        "subject.firstname": "Name",
        "subject.lastname": "Patient",
        "subject.sex": "other",
        "session.label": "StudyDescription",
        "session.age": 315576000,
        "session.weight": 30,
        "session.timestamp": "2021-03-12T11:12:02.700+01:00",
        "session.timezone": "Europe/Budapest",
        "acquisition.label": "7 - SeriesDescription",
        "acquisition.timestamp": "2021-03-12T11:27:43.700+01:00",
        "acquisition.timezone": "Europe/Budapest",
        "file.type": "spectroscopy",
    }

    rda.FieldA = "A_"
    rda.FieldB = {0: "B0_", 1: "B1_"}
    rda.FieldC[0, 0] = "C00_"
    del rda.FieldD
    rda.FieldE += 1
    rda.FieldF += 1

    file = io.BytesIO()
    rda.save(file)
    file.seek(0)
    rda = RDAFile(file)

    assert file.getvalue().endswith(b"\r\nSPECTRODATA")
    assert rda.FieldA == "A_"
    assert rda.FieldB == {0: "B0_", 1: "B1_"}
    assert rda.FieldC[0, 0] == "C00_"
    assert "FieldD" not in rda
    assert rda.FieldE == 2
    assert rda.FieldF == 2.1


def test_rda_errors():
    with pytest.raises(ValueError, match="cannot find header start"):
        RDAFile(io.BytesIO(b"foo"))

    with pytest.raises(ValueError, match="cannot find header end"):
        RDAFile(io.BytesIO(b">>> Begin of header <<<\n"))

    with pytest.raises(ValueError, match="cannot parse line"):
        RDAFile(io.BytesIO(b">>> Begin of header <<<\nfoo"))
