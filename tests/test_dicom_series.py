"""Tests for dicom.series module."""

import io
import logging
import os
import zipfile
from pathlib import Path
from unittest.mock import Mock

import pytest

from fw_file.dicom import DICOM, DICOMSeries, build_dicom_tree
from fw_file.dicom.config import get_config
from fw_file.dicom.testing import DICT_UNSET, create_dcm


@pytest.fixture
def dcm_dir(tmp_path):
    """Return temporary Path object with two test DICOMs."""
    create_dcm(SOPInstanceUID="1.2.3", file=str(tmp_path / "MR.1.2.3.dcm"))
    create_dcm(SOPInstanceUID="1.2.4", file=str(tmp_path / "MR.1.2.4.dcm"))
    return tmp_path


@pytest.fixture
def dcm_series(dcm_dir):
    """Return DICOMSeries loaded from 'dcm_dir'."""
    return DICOMSeries.from_dir(dcm_dir)


@pytest.fixture
def dcm_zip(dcm_dir):
    """Return ZIP archive path containing the DICOMs from 'dcm_dir'."""
    archive = dcm_dir / "series.dicom.zip"
    with zipfile.ZipFile(archive, "w") as zfile:
        for dcm in dcm_dir.glob("*.dcm"):
            zfile.write(dcm, dcm.name)
    return archive


@pytest.fixture
def create_dcm_files(tmp_path):
    def create_dcm_files_(*dcm_dicts):
        files = []
        for dcm_dict in dcm_dicts:
            dcm_dict.setdefault("file", f"{len(files)}.dcm")
            dcm_dict["file"] = tmp_path / dcm_dict["file"]
            dcm = create_dcm(**dcm_dict)
            files.append(str(dcm.filepath))
        return files

    return create_dcm_files_


def test_init_empty():
    series = DICOMSeries()
    assert len(series) == 0
    assert str(series) == "[]"


def test_init_files_and_data_model(tmp_path):
    file_str = str(tmp_path / "MR.1.2.3.dcm")
    create_dcm(SOPInstanceUID="1.2.3").save(file_str)
    file_path = tmp_path / "MR.1.2.4.dcm"
    create_dcm(SOPInstanceUID="1.2.4").save(file_path)
    file_buff = io.BytesIO()
    create_dcm(SOPInstanceUID="1.2.5").save(file_buff)
    file_buff.seek(0)
    file_dcm_str = create_dcm(SOPInstanceUID="1.2.6", file=tmp_path / "MR.1.2.6.dcm")
    file_dcm_buff = create_dcm(SOPInstanceUID="1.2.7")

    files = [file_str, file_path, file_buff, file_dcm_str, file_dcm_buff]
    series = DICOMSeries(*files)

    assert len(series) == 5
    for idx, dcm in enumerate(series):
        assert isinstance(dcm, DICOM)  # test deferred localpath loads

        assert dcm is series[idx]  # test iter and getitem sync # noqa
        assert dcm in series  # test iter and contains sync

    assert file_str in series  # str - str
    assert file_path in series  # path - str
    assert file_buff not in series  # buffer - no objectid/localpath
    assert file_dcm_str in series  # dcm - dcm (id)
    assert file_dcm_buff in series  # dcm - dcm (id)
    assert DICOM(file_str) in series  # dcm - str
    assert DICOM(file_path) in series  # dcm - str
    assert DICOM(file_dcm_str.filepath) in series  # dcm - dcm (path)


def test_init_with_filter(tmp_path):
    dcm1 = create_dcm(SeriesNumber=1, file=str(tmp_path / "test1.dcm"))
    dcm2 = create_dcm(SeriesNumber=1, file=str(tmp_path / "test2.dcm"))
    dcm3 = create_dcm(SeriesNumber=2, file=str(tmp_path / "test3.dcm"))

    def my_filter(dcm):
        return dcm.SeriesNumber == 1

    series = DICOMSeries(dcm1, dcm2, dcm3, filter_fn=my_filter)
    assert len(series) == 2
    assert dcm3 not in series


def test_getitem_with_slices_not_supported():
    series = DICOMSeries()
    with pytest.raises(NotImplementedError):
        _ = series[0:1]


def test_setitem_with_slices_not_supported():
    series = DICOMSeries()
    with pytest.raises(NotImplementedError):
        series[0:1] = ["a", "b"]


def test_setitem(tmp_path):
    create_dcm(SOPInstanceUID="1.2.3", file=str(tmp_path / "MR.1.2.3.dcm"))
    create_dcm(SOPInstanceUID="1.2.4", file=str(tmp_path / "MR.1.2.4.dcm"))
    series = DICOMSeries()
    series.append(tmp_path / "MR.1.2.3.dcm")
    series[0] = tmp_path / "MR.1.2.4.dcm"
    assert len(series) == 1
    assert series[0].filepath == str(tmp_path / "MR.1.2.4.dcm")


def test_from_dir(dcm_dir, tmp_path):
    series = DICOMSeries.from_dir(dcm_dir)
    dcm1 = str(tmp_path / "MR.1.2.3.dcm")
    dcm2 = str(tmp_path / "MR.1.2.4.dcm")
    assert str(series) == f"[DICOM('{dcm1}'), DICOM('{dcm2}')]"
    assert len(series) == 2


def test_from_dir_0_byte_file(dcm_dir, caplog):
    caplog.set_level(logging.WARNING)
    (dcm_dir / "test.dcm").touch()
    series = DICOMSeries.from_dir(dcm_dir)
    assert len(series) == 2
    assert len(caplog.record_tuples)
    assert caplog.record_tuples[0][1] == logging.WARNING
    assert "Ignoring" in caplog.record_tuples[0][2]


def test_from_dir_glob(tmp_path):
    subdir = tmp_path / "subdir"
    subdir.mkdir()
    dcm1 = str(subdir / "MR.4.5.6.dcm")
    dcm2 = str(subdir / "CT.7.8.9.dcm")
    create_dcm(SOPInstanceUID="4.5.6", file=dcm1)
    create_dcm(SOPInstanceUID="7.8.9", file=dcm2)

    series = DICOMSeries.from_dir(tmp_path)
    assert len(series) == 2
    assert dcm1 in series
    assert dcm2 in series

    series = DICOMSeries.from_dir(tmp_path, pattern="MR*")
    assert len(series) == 1
    assert dcm1 in series

    series = DICOMSeries.from_dir(tmp_path, recurse=False)
    assert not series


def test_from_zip(dcm_zip):
    with DICOMSeries.from_zip(dcm_zip) as series:
        tmpdir = series.dirpath
        assert len(series) == 2
        assert str((tmpdir / "MR.1.2.3.dcm").resolve()) in series
        assert str((tmpdir / "MR.1.2.4.dcm").resolve()) in series
    assert not tmpdir.exists()


def test_init_raises_on_invalid_dicom(tmp_path):
    txt = tmp_path / "test.txt"
    txt.write_text("not a dicom")
    with pytest.raises(ValueError):
        DICOMSeries.from_dir(tmp_path, defer_parse=False)


def test_bulk_get(dcm_series):
    assert dcm_series.bulk_get("SOPInstanceUID") == ["1.2.3", "1.2.4"]


def test_get(dcm_series):
    assert dcm_series.get("SeriesInstanceUID") == "1.2"


def test_get_first(dcm_series):
    assert dcm_series.get("SopInstanceUID", first=True) == "1.2.3"


def test_get_not_unique(dcm_series):
    with pytest.raises(ValueError):
        assert dcm_series.get("SOPInstanceUID")


def test_get_list_type(dcm_series):
    dcm_series.set("ImageOrientationPatient", [1.0, 2.0, 3.0, 4.0, 5.0, 6.0])
    assert dcm_series.get("ImageOrientationPatient") == [1.0, 2.0, 3.0, 4.0, 5.0, 6.0]


def test_set(dcm_series):
    dcm_series.set("SOPInstanceUID", "7.8.9")
    assert dcm_series.get("SOPInstanceUID") == "7.8.9"


def test_delete(dcm_series):
    dcm_series.delete("SOPInstanceUID")
    assert dcm_series.get("SOPInstanceUID") is None


def test_delete_missing_tag_does_not_raise(dcm_series):
    dcm_series.delete("PatientComments")


def test_convenience_methods_warn_on_cache_limit(dcm_series):
    get_config().instance_cache_size = 1

    with pytest.warns(UserWarning):
        dcm_series.bulk_get("SOPInstanceUID")
    with pytest.warns(UserWarning):
        dcm_series.get("SeriesInstanceUID")
    with pytest.warns(UserWarning):
        dcm_series.set("SOPInstanceUID", "7.8.9")
    with pytest.warns(UserWarning):
        dcm_series.delete("SOPInstanceUID")
    with pytest.warns(UserWarning):
        dcm_series.save()


def test_set_and_delete_raises_if_modification_is_not_persisted(dcm_dir):
    get_config().instance_cache_size = 1
    dcm_series = DICOMSeries.from_dir(dcm_dir, write_cache_drop=False)

    with pytest.raises(ValueError):
        dcm_series.set("SOPInstanceUID", "7.8.9")
    with pytest.raises(ValueError):
        dcm_series.delete("SOPInstanceUID")


def test_instance_cache(dcm_dir):
    get_config().instance_cache_size = 1
    dcm_series = DICOMSeries.from_dir(dcm_dir)

    file1 = str(dcm_dir / "MR.1.2.3.dcm")
    assert dcm_series[0].filepath == file1
    assert list(dcm_series.instance_cache.keys()) == [file1]

    file2 = str(dcm_dir / "MR.1.2.4.dcm")
    assert dcm_series[1].filepath == file2
    assert list(dcm_series.instance_cache.keys()) == [file2]


def test_apply(dcm_series):
    def callback(dcm):
        del dcm.PatientID
        dcm.PatientName = "deid"

    dcm_series.apply(callback)
    assert dcm_series.get("PatientID") is None
    assert dcm_series.get("PatientName") == "deid"


def test_save(dcm_series, tmp_path):
    dcm_series.set("SOPInstanceUID", "7.8.9")
    dcm_series.save()
    series = DICOMSeries.from_dir(tmp_path)
    assert series.bulk_get("SOPInstanceUID") == ["7.8.9", "7.8.9"]


def test_save_read_only(dcm_series, config):
    config.read_only = True
    dcm_series.set("SOPInstanceUID", "7.8.9")
    with pytest.raises(TypeError):
        dcm_series.save()


def test_to_dir(dcm_series, tmp_path):
    output = tmp_path / "output"
    output.mkdir()
    dcm_series.to_dir(str(output))
    assert set(p.name for p in output.glob("*")) == {"1.2.3.MR.dcm", "1.2.4.MR.dcm"}


def test_to_dir_name_fn(dcm_series, tmp_path):
    def name_fn(dcm):
        if type(dcm.file) is Path:
            return dcm.file.name
        elif type(dcm.file) is str:
            return Path(dcm.file).name
        else:
            return Path(dcm.file.file.name).name

    output = tmp_path / "output"
    output.mkdir()
    dcm_series.to_dir(str(output), instance_name_fn=lambda dcm: name_fn(dcm))
    assert set(p.name for p in output.glob("*")) == {"MR.1.2.3.dcm", "MR.1.2.4.dcm"}


def test_to_dir_read_only(dcm_series, tmp_path, config):
    config.read_only = True
    output = tmp_path / "output"
    output.mkdir()
    with pytest.raises(TypeError):
        dcm_series.to_dir(str(output))


def test_to_dir_on_progress(dcm_series, tmp_path):
    output = tmp_path / "output"
    output.mkdir()
    on_progress = Mock()
    dcm_series.to_dir(output, on_progress=on_progress)
    assert set(p.name for p in output.glob("*")) == {"1.2.3.MR.dcm", "1.2.4.MR.dcm"}
    assert on_progress.call_count == 2


def test_to_zip(dcm_series, tmp_path, mocker):
    output_zip = tmp_path / "output.zip"

    dcm_series.to_zip(output_zip, comment="foo")
    with zipfile.ZipFile(output_zip) as zipf:
        assert zipf.comment == b"foo"
        assert set(zipf.namelist()) == {"1.2.3.MR.dcm", "1.2.4.MR.dcm"}

    dcm_series.to_zip(output_zip, comment=b"bar")
    with zipfile.ZipFile(output_zip) as zipf:
        assert zipf.comment == b"bar"
        assert set(zipf.namelist()) == {"1.2.3.MR.dcm", "1.2.4.MR.dcm"}

    # checking default compression
    zipfile_mock = mocker.patch("fw_file.dicom.series.zipfile.ZipFile")
    dcm_series.to_zip(output_zip)
    assert zipfile_mock.call_args[1]["compression"] == zipfile.ZIP_DEFLATED


def test_to_zip_on_progress(dcm_series, tmp_path):
    output = tmp_path / "output"
    on_progress = Mock()
    dcm_series.to_zip(output, on_progress=on_progress)
    assert on_progress.call_count == 2
    with zipfile.ZipFile(output) as zipf:
        assert set(zipf.namelist()) == {"1.2.3.MR.dcm", "1.2.4.MR.dcm"}


def test_to_zip_name_fn(dcm_series, tmp_path):
    def name_fn(dcm):
        if type(dcm.file) is Path:
            return dcm.file.name
        elif type(dcm.file) is str:
            return Path(dcm.file).name
        else:
            return Path(dcm.file.file.name).name

    output = tmp_path / "output"
    dcm_series.to_zip(output, instance_name_fn=lambda dcm: name_fn(dcm))
    with zipfile.ZipFile(output) as zipf:
        assert set(zipf.namelist()) == {"MR.1.2.3.dcm", "MR.1.2.4.dcm"}


@pytest.fixture
def tmp_path_cwd(tmp_path):
    orig_cwd = os.getcwd()
    os.chdir(tmp_path)
    yield tmp_path
    os.chdir(orig_cwd)


def test_to_upload_dir_defaults_to_cwd(dcm_series, tmp_path_cwd):
    filepath, _ = dcm_series.to_upload()
    assert filepath.exists()
    assert filepath.parent == tmp_path_cwd


def test_to_upload_dir_string(dcm_series, tmp_path):
    filepath, _ = dcm_series.to_upload(str(tmp_path))
    assert filepath.exists()
    assert filepath.parent == tmp_path


def test_to_upload_multi_file(dcm_series, tmp_path):
    filepath, metadata = dcm_series.to_upload(tmp_path)
    assert filepath.exists()
    assert filepath.parent == tmp_path
    assert filepath.name == "1.2.dicom.zip"
    assert metadata["file.zip_member_count"] == 2


def test_to_upload_multi_file_on_progress(dcm_series, tmp_path):
    on_progress = Mock()
    filepath, _ = dcm_series.to_upload(tmp_path, on_progress=on_progress)
    assert filepath.exists()
    assert on_progress.call_count == 2


def test_to_upload_single_file(dcm_series, tmp_path):
    dcm_series.pop()
    filepath, metadata = dcm_series.to_upload(tmp_path)
    assert filepath.exists()
    assert filepath.parent == tmp_path
    assert filepath.name == "1.2.dcm"
    assert "file.zip_member_count" not in metadata


def test_to_upload_single_file_on_progress(dcm_series, tmp_path):
    dcm_series.pop()
    on_progress = Mock()
    filepath, _ = dcm_series.to_upload(tmp_path, on_progress=on_progress)
    assert filepath.exists()
    assert on_progress.call_count == 1


def test_sort(dcm_series):
    dcm_series.sort()
    dcm_series[0].SeriesInstanceUID = "1.3"
    dcm_series[0].SOPInstanceUID = "1.3.4"
    dcm_series.sort()
    assert dcm_series[0].SeriesInstanceUID == "1.2"
    assert dcm_series[0].SOPInstanceUID == "1.2.4"
    assert dcm_series[1].SeriesInstanceUID == "1.3"
    assert dcm_series[1].SOPInstanceUID == "1.3.4"


def test_sort_key(dcm_series):
    dcm_series[0].InstanceNumber = 2
    dcm_series[1].InstanceNumber = 1
    dcm_series.sort(key=lambda dcm: dcm.InstanceNumber)
    assert dcm_series[0].InstanceNumber == 1
    assert dcm_series[1].InstanceNumber == 2


def test_meta(tmp_path):
    file = tmp_path / "1.dcm"
    create_dcm(
        PatientID="test",
        StudyDescription="Study desc",
        StudyInstanceUID="1",
        SeriesDescription="Series desc",
        SeriesInstanceUID="1.2",
        file=file,
    )
    series = DICOMSeries(file)
    assert series.get_meta() == {
        "subject.label": "test",
        "session.uid": "1",
        "session.label": "Study desc",
        "acquisition.uid": "1.2",
        "acquisition.label": "Series desc",
        "file.type": "dicom",
    }


def test_meta_conflict(create_dcm_files):
    files = create_dcm_files({"PatientID": "foo"}, {"PatientName": "bar"})
    series = DICOMSeries(*files)
    with pytest.raises(ValueError):
        series.get_meta()


def test_meta_conflict_on_acquisition_timestamp(create_dcm_files):
    files = create_dcm_files(
        {"AcquisitionDateTime": "19991231235958.123"},
        {"AcquisitionDateTime": "19991231235959.456"},
    )
    series = DICOMSeries(*files)
    expected_timestamp = "1999-12-31T23:59:58.123+01:00"
    assert str(series.get_meta()["acquisition.timestamp"]) == expected_timestamp


def test_meta_empty_series():
    with pytest.raises(ValueError):
        DICOMSeries().get_meta()


def test_build_dicom_tree(create_dcm_files):
    files = create_dcm_files(
        {
            "StudyInstanceUID": "1",
            "SeriesInstanceUID": "1.2",
            "SOPInstanceUID": "1.2.3",
        },
        {
            "StudyInstanceUID": "1",
            "SeriesInstanceUID": "1.2",
            "SOPInstanceUID": "1.2.4",
        },
    )
    tree, errors = build_dicom_tree(files)
    assert tree == {"1": {"1.2": {"1.2.3": files[0], "1.2.4": files[1]}}}
    assert errors == []


def test_build_dicom_tree_with_fw_storage(create_dcm_files, tmp_path):
    try:
        from fw_storage import get_storage
    except ImportError:
        pytest.skip("skipping fw-storage test (not installed)")
    (tmp_path / "storage").mkdir()
    fs = get_storage(f"fs://{tmp_path}/storage")
    files = create_dcm_files(
        {
            "StudyInstanceUID": "1",
            "SeriesInstanceUID": "1.2",
            "SOPInstanceUID": "1.2.3",
        },
    )
    fs.set("1.dcm", files[0])
    tree, (error,) = build_dicom_tree(["1.dcm", "2.dcm"], open_fn=fs.get)
    assert tree == {"1": {"1.2": {"1.2.3": "1.dcm"}}}
    assert error.file == "2.dcm"
    assert "No such file" in error.message


def test_build_dicom_tree_invalid_path():
    tree, (err,) = build_dicom_tree(["invalid_path.txt"])
    assert tree == {}
    assert err.file == "invalid_path.txt"
    assert "No such file" in err.message


def test_build_dicom_tree_implicit_force(tmp_path):
    txt = tmp_path / "test.txt"
    txt.write_text("not a dicom")
    tree, (err,) = build_dicom_tree([txt])
    assert tree == {}
    assert err.file == str(txt)
    # pydicom returns empty dataset with force=True
    assert "Missing study key" in err.message


def test_build_dicom_tree_invalid_file_disable_force(tmp_path):
    txt = tmp_path / "test.txt"
    txt.write_text("not a dicom")
    tree, (err,) = build_dicom_tree([txt], force=False)
    assert tree == {}
    assert err.file == str(txt)
    assert "Invalid DICOM" in err.message


@pytest.mark.parametrize(
    "dcm_dict,expected",
    [
        ({"StudyInstanceUID": DICT_UNSET}, "Missing study key"),
        ({"SeriesInstanceUID": DICT_UNSET}, "Missing series key"),
        ({"SOPInstanceUID": DICT_UNSET}, "Missing instance key"),
    ],
)
def test_build_dicom_tree_missing_key_error(dcm_dict, expected, tmp_path):
    file = tmp_path / "1.dcm"
    # MediaStorageSOPInstanceUID is required and defaults to SopInstanceUID
    dcm_dict["file_meta"] = {"MediaStorageSOPInstanceUID": "1.2.3"}
    create_dcm(**dcm_dict, file=file)
    tree, (err,) = build_dicom_tree([file])
    assert tree == {}
    assert err.file == str(file)
    assert expected in err.message


def test_build_dicom_tree_instance_conflict(create_dcm_files):
    files = create_dcm_files(
        {
            "StudyInstanceUID": "1",
            "SeriesInstanceUID": "1.2",
            "SOPInstanceUID": "1.2.3",
        },
        {
            "StudyInstanceUID": "1",
            "SeriesInstanceUID": "1.2",
            "SOPInstanceUID": "1.2.3",
        },
        {
            "StudyInstanceUID": "2",
            "SeriesInstanceUID": "2.2",
            "SOPInstanceUID": "1.2.3",
        },
    )
    tree, (err1, err2) = build_dicom_tree(files)
    assert tree == {"1": {"1.2": {"1.2.3": files[0]}}}
    assert err1.file == files[1]
    assert "Instance 1/1.2/1.2.3 conflicts with instance 1/1.2/1.2.3" in err1.message
    assert err2.file == files[2]
    assert "Instance 2/2.2/1.2.3 conflicts with instance 1/1.2/1.2.3" in err2.message


def test_build_dicom_tree_series_conflict(create_dcm_files):
    files = create_dcm_files(
        {"StudyInstanceUID": "1", "SeriesInstanceUID": "1.2"},
        {"StudyInstanceUID": "2", "SeriesInstanceUID": "1.2"},
    )
    tree, (err,) = build_dicom_tree(files)
    assert tree == {"1": {"1.2": {"1.2.3": files[0]}}}
    assert err.file == files[1]
    assert "Series 2/1.2 is already associated with study 1" in err.message
