"""Tests for fw_file_json class."""

import json as pyjson
from pathlib import Path

import pytest
from dotty_dict import dotty

from fw_file.json import JSON

INVALID_JSON = "invalid.json"
VALID_JSON = "valid.json"
ASSETS_ROOT = Path(__file__).parent / "assets"

VALID_JSON_DATA = {
    "string_key": "string",
    "num_key": 123,
    "empty_key_remove": {"empty_key_nest": ""},
    "nested_key_parent": {"nested_key1": {"nested_key2": "nested2_val"}},
}

VALID_JSON_KEYS = [
    "nested_key_parent.nested_key1.nested_key2",
    "empty_key_remove.empty_key_nest",
    "nested_key_parent.nested_key1",
    "string_key",
    "num_key",
    "empty_key_remove",
    "nested_key_parent",
]


VALID_JSON_NOBLANKS = dotty(
    {
        "string_key": "string",
        "num_key": 123,
        "nested_key_parent": {"nested_key1": {"nested_key2": "nested2_val"}},
    }
)


@pytest.fixture
def json_file(tmp_path):
    """Returns path to dicom file in assets."""

    def get_json_file(filename):
        tmp_file_path = tmp_path / filename
        with open(tmp_file_path, "w") as tmp_file:
            pyjson.dump(VALID_JSON_DATA, tmp_file)

        return tmp_file_path

    return get_json_file


@pytest.fixture
def fw_json(json_file):
    """returns a flywheel json object"""
    fw_json = JSON(json_file(VALID_JSON))
    return fw_json


def test_json_init_from_invalid_file(tmp_path):
    invalid_json_file = tmp_path / "invalid.json"
    invalid_json_file.write_text('{\'key1\': {"key2": "val2"}}')

    with pytest.raises(pyjson.decoder.JSONDecodeError):
        JSON(invalid_json_file)


def test_json_init_from_file(fw_json):
    assert fw_json.fields == VALID_JSON_DATA


def test_json_save(fw_json, tmp_path):
    path = tmp_path / "destination.json"
    fw_json.save(path)
    assert path.exists()


def test_json_meta(fw_json):
    assert fw_json.get_meta() == {
        "file.name": "valid.json",
        "file.type": "source code",
    }


def test_json_get(fw_json):
    assert fw_json.get("string_key") == "string"


def test_json_set(fw_json):
    fw_json["magic"] = b"n+2"
    assert fw_json.get("magic") == b"n+2"

    assert fw_json["nested_key_parent.nested_key1.nested_key2"] == "nested2_val"
    fw_json["nested_key_parent.nested_key1.nested_key2"] = "new_nested_value"
    assert fw_json["nested_key_parent.nested_key1.nested_key2"] == "new_nested_value"
    assert fw_json["nested_key_parent"] == {
        "nested_key1": {"nested_key2": "new_nested_value"}
    }


def test_json_del(fw_json):
    del fw_json["num_key"]
    assert "num_key" not in fw_json.fields

    del fw_json["nested_key_parent.nested_key1.nested_key2"]
    assert fw_json["nested_key_parent.nested_key1"] == {}


def test_json_len(fw_json):
    assert len(fw_json) == len(VALID_JSON_DATA)


def test_json_iter(fw_json):
    assert len(list(fw_json)) == len(VALID_JSON_DATA)


def test_get_all_keys_sorted_deepest_first(fw_json):
    sorted_keys = fw_json.get_all_keys()
    assert sorted_keys == VALID_JSON_KEYS


def test_to_dict(fw_json):
    assert fw_json.to_dict() == VALID_JSON_DATA


def test_remove_blanks_from_data(fw_json):
    fw_json.remove_blanks()
    assert fw_json.fields == VALID_JSON_NOBLANKS
    assert fw_json.removed == {"empty_key_remove", "empty_key_remove.empty_key_nest"}
