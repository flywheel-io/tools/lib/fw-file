"""Tests for the dicom module."""

# ruff: noqa: E721
import io
import pathlib
from datetime import datetime

import pytest
from fw_utils import get_tzinfo
from pydicom import DataElement, tag
from pydicom.dataelem import RawDataElement

from fw_file.dicom import dicom, get_private_tag
from fw_file.dicom.testing import DCM_DEFAULT, DICT_UNSET, create_dcm


def test_init_raises_on_defer_size_with_nonlocal_file():
    with pytest.raises(ValueError):
        dicom.DICOM(io.BytesIO(b"test"), defer_size=512)


def test_init_zero_byte_file():
    io_file = io.BytesIO()
    with pytest.raises(ValueError) as err:
        dicom.DICOM(io_file)
    assert "BinFile" in err.value.args[0]


def test_public_tag_dict_get():
    name = "Doe^John"
    dcm = create_dcm(PatientName=name)
    assert dcm["PatientName"] == name
    assert dcm["patientname"] == name
    assert dcm["00100010"] == name
    assert dcm["0010", "0010"] == name
    assert dcm[0x00100010] == name
    assert dcm[0x0010, 0x0010] == name


def test_public_tag_dict_get_missing_raises():
    dcm = create_dcm()
    with pytest.raises(KeyError):
        dcm["PatientName"]
    with pytest.raises(KeyError):
        dcm["patientname"]
    with pytest.raises(KeyError):
        dcm["00100010"]
    with pytest.raises(KeyError):
        dcm["0010", "0010"]
    with pytest.raises(KeyError):
        dcm[0x00100010]
    with pytest.raises(KeyError):
        dcm[0x0010, 0x0010]


def test_public_tag_dict_set():
    name = "Doe^John"
    dcm = create_dcm()
    dcm["PatientName"] = name
    assert dcm["PatientName"] == name


def test_public_tag_dict_del():
    dcm = create_dcm(PatientName="Doe^John")
    del dcm["PatientName"]
    with pytest.raises(KeyError):
        dcm["PatientName"]
    with pytest.raises(KeyError):
        del dcm["PatientName"]


def test_public_tag_attr_get():
    name = "Doe^John"
    dcm = create_dcm(PatientName=name)
    assert dcm.PatientName == name
    assert dcm.patientname == name


def test_public_tag_attr_get_missing_raises():
    dcm = create_dcm()
    with pytest.raises(AttributeError):
        dcm.PatientName
    with pytest.raises(AttributeError):
        dcm.patientname


def test_public_tag_attr_set():
    name = "Doe^John"
    dcm = create_dcm()
    dcm.PatientName = name
    assert dcm.PatientName == name


def test_public_tag_attr_del():
    dcm = create_dcm(PatientName="Doe^John")
    del dcm.PatientName
    with pytest.raises(AttributeError):
        dcm.PatientName
    with pytest.raises(AttributeError):
        del dcm.PatientName


# use a private creator and a tag chosen from the pydicom-shipped dict for tests
# https://github.com/pydicom/pydicom/blob/v2.1.2/pydicom/_private_dict.py#L211
private_dcmdict = {
    "00190010": ("LO", "AGFA"),  # creator
    "00191015": ("LO", "test"),  # Dose Monitoring List
}


def test_private_tag_dict_get():
    dcm = create_dcm(**private_dcmdict)
    assert dcm["00190010"] == "AGFA"
    assert dcm["AGFA", "Dose Monitoring List"] == "test"
    assert dcm["AGFA", "dosemonitoringlist"] == "test"
    assert dcm["AGFA", "0019xx15"] == "test"
    assert dcm["00191015"] == "test"  # normal tag
    with pytest.raises(KeyError):
        # implicit creator is not enabled by default
        dcm["Dose Monitoring List"]


def test_private_tag_dict_get_with_implicit_creator(config):
    config.implicit_creator = True
    dcm = create_dcm(**private_dcmdict)
    assert dcm["Dose Monitoring List"] == "test"  # globally unique keyword
    assert dcm["0019xx15"] == "test"  # dataset-context-unique xtag


def test_private_tag_dict_get_unknown_tag():
    dcmdict = {
        "00990010": ("LO", "Foo"),  # creator
        "00991010": ("LO", "Bar"),  # tag
    }
    dcm = create_dcm(**dcmdict)
    assert dcm["Foo", "0099xx10"] == "Bar"
    assert dcm["00991010"] == "Bar"


def test_private_tag_dict_get_raises_on_multiple_xtag_candidates():
    dcm = create_dcm()
    with pytest.raises(KeyError):
        # https://github.com/pydicom/pydicom/blob/v2.1.2/pydicom/_private_dict.py#L48
        dcm["ACUSON", "Unknown"]  # unknown keyword is not unique in creator


def test_private_tag_dict_get_raises_on_multiple_xtag_w_same_keyword():
    dcmdict = {
        "00090010": ("LO", "ACUSON"),  # creator
        "00091000": ("IS", "1"),  # tag
        "00091001": ("IS", "2"),  # tag
    }
    dcm = create_dcm(**dcmdict)
    with pytest.raises(KeyError):
        # https://github.com/pydicom/pydicom/blob/v2.1.2/pydicom/_private_dict.py#L48
        dcm["ACUSON", "Unknown"]  # unknown keyword is not unique in creator


def test_private_tag_dict_set():
    dcm = create_dcm()
    dcm["AGFA", "Dose Monitoring List"] = "test"
    assert dcm["AGFA", "Dose Monitoring List"] == "test"


def test_private_tag_dict_set_unknown_tag():
    dcmdict = {
        "00990010": ("LO", "Foo"),  # creator
        "00991010": ("LO", "Bar"),  # tag
    }
    dcm = create_dcm(**dcmdict)
    dcm["00991010"] = "Baz"
    assert dcm["00991010"] == "Baz"


def test_private_tag_dict_set_unknown_tag_w_creator_and_xtag():
    dcmdict = {
        "00990010": ("LO", "Foo"),  # creator
        "00991010": ("LO", "Bar"),  # tag
    }
    dcm = create_dcm(**dcmdict)
    dcm["Foo", "0099xx10"] = "Baz"
    assert dcm["Foo", "0099xx10"] == "Baz"


def test_private_tag_dict_set_raises_on_unknown_non_existing_tag():
    dcm = create_dcm()
    with pytest.raises(KeyError):
        dcm["00991010"] = "Baz"


def test_private_tag_dict_set_raises_on_multiple_creator_candidates(config):
    config.implicit_creator = True
    dcm = create_dcm()
    with pytest.raises(KeyError):
        dcm["0019xx10"]


def test_private_tag_dict_set_raises_when_no_blocks_are_available():
    creators = {
        f"001900{block:02x}": ("LO", f"Creator {block}") for block in range(0x10, 0x100)
    }
    dcm = create_dcm(**creators)
    with pytest.raises(KeyError):
        dcm["AGFA", "Dose Monitoring List"] = "test"


def test_private_tag_dict_del():
    dcm = create_dcm(**private_dcmdict)
    del dcm["AGFA", "Dose Monitoring List"]
    with pytest.raises(KeyError):
        dcm["AGFA", "Dose Monitoring List"]
    with pytest.raises(KeyError):
        del dcm["AGFA", "Dose Monitoring List"]


def test_file_meta():
    ct, mr = "1.2.840.10008.5.1.4.1.1.2", "1.2.840.10008.5.1.4.1.1.4"
    file_meta = {"MediaStorageSOPClassUID": ct}
    dcm = create_dcm(file_meta=file_meta)
    assert dcm.MediaStorageSOPClassUID == ct

    dcm.MediaStorageSOPClassUID = mr
    assert dcm.MediaStorageSOPClassUID == mr

    del dcm.MediaStorageSOPClassUID
    with pytest.raises(AttributeError):
        del dcm.MediaStorageSOPClassUID


def test_get_value_type():
    dcm = create_dcm(
        StudyInstanceUID="1",  # UI
        StudyDate="19991231",  # DA
        StudyTime="235959",  # TM
        AcquisitionDateTime="19991231235959",  # DT
        PatientName="Doe^John",  # PN
        PatientSize=1.2,  # DS
        Allergies="nuts\\bolts",  # LO + MultiValue
        StageNumber=1,  # IS
        ExaminedBodyThickness=1.2,  # FL
        SelectorUNValue=("UN", b"foo\\bar"),  # UN
        LanguageCodeSequence=("SQ", [{"CodeValue": "1"}]),  # SQ
        **{
            "00090010": ("LO", "ACUSON"),
            "00091002": ("UN", b"\xff"),  # UN private tag w/ non-unicode
        },
    )

    # using exact type comparison on purpose
    assert type(dcm.StudyInstanceUID) is str
    assert type(dcm.StudyDate) is str
    assert type(dcm.StudyTime) is str
    assert type(dcm.AcquisitionDateTime) is str
    assert type(dcm.PatientName) is str
    assert type(dcm.PatientSize) is float
    assert type(dcm.Allergies) is list
    assert type(dcm.StageNumber) is int
    assert type(dcm.ExaminedBodyThickness) is float
    assert type(dcm.SelectorUNValue) is list
    assert type(dcm.LanguageCodeSequence) is list
    assert type(dcm["ACUSON", "0009xx02"]) is bytes

    assert dcm.StudyInstanceUID == "1"
    assert dcm.StudyDate == "19991231"
    assert dcm.StudyTime == "235959"
    assert dcm.AcquisitionDateTime == "19991231235959"
    assert dcm.PatientName == "Doe^John"
    assert dcm.PatientSize == 1.2
    assert dcm.Allergies == ["nuts", "bolts"]
    assert dcm.StageNumber == 1
    assert abs(dcm.ExaminedBodyThickness - 1.2) < 0.0000001
    assert dcm.SelectorUNValue == ["foo", "bar"]
    assert dcm.LanguageCodeSequence[0].CodeValue == "1"
    assert dcm["ACUSON", "0009xx02"] == b"\xff"


def test_defer_size(tmp_path):
    file1 = tmp_path / "file1.dcm"
    file2 = tmp_path / "file2.dcm"
    create_dcm(PixelData=bytes(1024), file=file1)
    dicom.DICOM(file1, defer_size=512).save(file2)
    assert file1.read_bytes() == file2.read_bytes()


def test_stop_when(tmp_path):
    filepath = str(tmp_path / "test.dcm")
    dcm1 = create_dcm(PixelData=bytes(64), file=filepath)
    assert "PixelData" in dcm1

    dcm2 = dicom.DICOM(filepath, stop_when="PixelData")
    assert "PixelData" not in dcm2


def test_stop_before_pixel(tmp_path):
    filepath = str(tmp_path / "test.dcm")
    dcm1 = create_dcm(PixelData=bytes(64), file=filepath)
    assert "PixelData" in dcm1

    dcm2 = dicom.DICOM(filepath, stop_before_pixels=True)
    assert "PixelData" not in dcm2


def test_stop_before_pixel_mutual_exclusivity(tmp_path):
    filepath = str(tmp_path / "test.dcm")
    create_dcm(PixelData=bytes(1024), file=filepath)
    with pytest.raises(ValueError):
        dicom.DICOM(filepath, stop_when="PixelData", stop_before_pixels=True)


def test_iter():
    # TODO finalize and test the type yielded - FullTag?
    dcm = create_dcm()
    assert len(list(iter(dcm))) == len(DCM_DEFAULT)


def test_len():
    dcm = create_dcm()
    assert len(dcm) == len(DCM_DEFAULT)


def test_compare():
    dcm1 = create_dcm()
    dcm2 = create_dcm()
    dcm3 = create_dcm(SOPInstanceUID="1.2.5")
    assert dcm1 == dcm2
    assert dcm1 < dcm3
    assert dcm2 < dcm3


def test_compare_raises_when_object_type_is_incompatible():
    dcm = create_dcm()
    with pytest.raises(TypeError):
        dcm == 1
    with pytest.raises(TypeError):
        dcm < 1


def test_decode_on_instantiation(tmp_path):
    filepath = str(tmp_path / "test.dcm")
    create_dcm(file=filepath)
    dcm = dicom.DICOM(filepath, decode=True)
    assert dcm.dataset.raw._dict[tag.BaseTag(0x00100020)].is_raw is False


def test_decode(tmp_path):
    filepath = str(tmp_path / "test.dcm")
    create_dcm(file=filepath)
    dcm = dicom.DICOM(filepath)
    assert dcm.dataset.raw._dict[tag.BaseTag(0x00100020)].is_raw is True
    assert dcm.read_context is not None
    # read_partial uses tags from file_meta
    orig_data_elem_len = len(dcm.read_context.data_elements)
    dcm.decode()
    assert dcm.read_context is not None
    assert len(dcm.read_context.data_elements) == orig_data_elem_len + 6


def test_decode_single_element(tmp_path):
    filepath = str(tmp_path / "test.dcm")
    create_dcm(file=filepath)

    dcm = dicom.DICOM(filepath)
    assert dcm.read_context is not None
    # read_partial uses tags from file_meta
    orig_data_elem_len = len(dcm.read_context.data_elements)

    _ = dcm.get("00100020")
    assert len(dcm.read_context.data_elements) == orig_data_elem_len + 1


def test_update_original_attributes(tmp_path):
    filepath = str(tmp_path / "test.dcm")
    create_dcm(file=filepath)
    dcm = dicom.DICOM(filepath)
    assert dcm.dataset.raw._dict[tag.BaseTag(0x00100020)].is_raw is True
    assert dcm.read_context is not None
    # read_partial uses tags from file_meta
    orig_data_elem_len = len(dcm.read_context.data_elements)
    dcm.decode()
    assert dcm.read_context is not None
    assert len(dcm.read_context.data_elements) == orig_data_elem_len + 6
    dcm.update_orig_attrs()
    assert dcm.read_context is not None
    assert len(dcm.read_context.data_elements) == 0


def test_update_original_attributes_saves_validation_mode_2(tmp_path, config):
    config.reading_validation_mode = 2
    filepath = str(tmp_path / "test.dcm")
    create_dcm(file=filepath)
    dcm = dicom.DICOM(filepath)
    # Manually add an invalid RawDataElement
    b_day_tag = tag.BaseTag(0x00100030)
    b_day_val = b"anonymized"
    dcm.dataset.raw._dict[b_day_tag] = RawDataElement(
        b_day_tag, "DA", len(b_day_val), b_day_val, 10, True, True
    )
    assert dcm.dataset.raw._dict[tag.BaseTag(0x00100020)].is_raw is True
    assert dcm.read_context is not None
    # read_partial uses tags from file_meta
    orig_data_elem_len = len(dcm.read_context.data_elements)
    dcm.decode()
    assert dcm.read_context is not None
    assert len(dcm.read_context.data_elements) == orig_data_elem_len + 7
    dcm.save(tmp_path / "test2.dcm")
    dcm2 = dicom.DICOM(tmp_path / "test2.dcm")
    dcm2.decode()
    dcm2.update_orig_attrs()
    # When validation_mode == 2,
    # values are modified to adhere to DICOM standard.
    # OriginalAttributesSequence stores info about these changes
    assert len(dcm2.OriginalAttributesSequence) == 1
    orig = dcm2.OriginalAttributesSequence[0]
    assert orig.ModifyingSystem == "fw_file"
    assert orig.ModifiedAttributesSequence[0].PatientBirthDate == ""
    nonconform = orig.NonconformingModifiedAttributesSequence
    assert len(nonconform) == 1
    assert nonconform[0].SelectorAttribute == b_day_tag
    assert (
        nonconform[0].get_dataelem("NonconformingDataElementValue").value == b_day_val
    )


def test_update_original_attributes_saves_validation_mode_1(tmp_path, config):
    config.reading_validation_mode = 1
    filepath = str(tmp_path / "test.dcm")
    create_dcm(file=filepath)
    dcm = dicom.DICOM(filepath)
    # Manually add an invalid RawDataElement
    b_day_tag = tag.BaseTag(0x00100030)
    b_day_val = b"anonymized"
    dcm.dataset.raw._dict[b_day_tag] = RawDataElement(
        b_day_tag, "DA", len(b_day_val), b_day_val, 10, True, True
    )
    assert dcm.dataset.raw._dict[tag.BaseTag(0x00100020)].is_raw is True
    assert dcm.read_context is not None
    # read_partial uses tags from file_meta
    orig_data_elem_len = len(dcm.read_context.data_elements)
    dcm.decode()
    assert dcm.read_context is not None
    assert len(dcm.read_context.data_elements) == orig_data_elem_len + 7
    dcm.save(tmp_path / "test2.dcm")
    dcm2 = dicom.DICOM(tmp_path / "test2.dcm")
    dcm2.decode()
    dcm2.update_orig_attrs()
    # If validation_mode == 1, fixers don't change anything
    # so OriginalAttributesSequence is not applicable.
    assert not hasattr(dcm2, "OriginalAttributesSequence")


def test_save(tmp_path):
    dcm = create_dcm()
    filepath = tmp_path / "test.dcm"
    dcm.save(filepath)
    dcm_bytes = filepath.read_bytes()
    assert dcm_bytes.startswith(b"\x00" * 128 + b"DICM")


def test_save_read_only(tmp_path, config):
    config.read_only = True
    dcm = create_dcm()
    filepath = tmp_path / "test.dcm"
    with pytest.raises(TypeError):
        dcm.save(filepath)


def test_save_raises_on_invalid_file():
    dcm = create_dcm()
    with pytest.raises(ValueError):
        dcm.save(None)


def test_dir():
    dcm = create_dcm()
    assert dcm.dir() == sorted(list(DCM_DEFAULT.keys()))


def test_walk():
    dcm = create_dcm()
    trace = []

    def dummy_callback(dataset, elem) -> None:
        trace.append(elem.keyword)

    dcm.walk(dummy_callback)

    assert set(trace) == set(dcm.dir())


def test_get_dataelem():
    dcm = create_dcm()
    de = dcm.get_dataelem("PatientID")
    assert isinstance(de, DataElement)
    assert de.value == "test"


def test_init_raises_on_invalid_file():
    with pytest.raises(ValueError):
        dicom.DICOM(None)


def test_filepath(tmp_path):
    dcm_1 = create_dcm()  # BytesIO
    assert dcm_1.filepath is None

    filepath = str(tmp_path / "test.dcm")  # str
    dcm_2 = create_dcm(file=filepath)
    assert dcm_2.filepath == filepath

    dcm_3 = dicom.DICOM(pathlib.Path(filepath))  # Path
    assert dcm_3.filepath == filepath


def test_str(tmp_path):
    filepath = str(tmp_path / "test.dcm")
    dcm = create_dcm(file=filepath)
    assert str(dcm) == f"DICOM('{filepath}')"


def test_attr_proxy():
    dcm = create_dcm()
    assert callable(dcm.dataset.save_as)
    with pytest.raises(AttributeError):
        getattr(dcm.dataset, "foo")


def test_get_private_tag_raises_without_creator():
    with pytest.raises(ValueError):
        get_private_tag("11111111")


def test_get_private_tag_raises_with_unknown_creator():
    with pytest.raises(ValueError):
        get_private_tag("111xx11", creator="Foo")


def test_meta():
    dcm = create_dcm(
        OperatorsName=("PN", ["Op^Mike", "Second^Operator"]),
        PatientAge="1D",
        PatientID="test",
        PatientName="Doe^John",
        PatientSex="F",
        PatientWeight="79.9",
        StudyDate="19991231",
        StudyDescription="Study desc",
        StudyInstanceUID="1",
        StudyTime="235959.0999",
        SeriesDescription="Series desc",
        SeriesInstanceUID="1.2",
    )
    assert dcm.get_meta() == {
        "subject.label": "test",
        "subject.firstname": "John",
        "subject.lastname": "Doe",
        "subject.sex": "female",
        "session.age": 86400,
        "session.weight": 79.9,
        "session.uid": "1",
        "session.label": "Study desc",
        "session.operator": "Mike Op, Operator Second",
        "session.timestamp": "1999-12-31T23:59:59.099+01:00",
        "session.timezone": "Europe/Budapest",
        "acquisition.uid": "1.2",
        "acquisition.label": "Series desc",
        "acquisition.timestamp": "1999-12-31T23:59:59.099+01:00",
        "acquisition.timezone": "Europe/Budapest",
        "file.name": "1.2.3.MR.dcm",
        "file.type": "dicom",
    }


SD, ST, TZ = "StudyDate", "StudyTime", "TimezoneOffsetFromUTC"
TEST_GET_SESSION_TIMESTAMP_ARGS = [
    ({}, None),
    ({SD: "invalid"}, None),
    ({SD: "19991232"}, None),
    ({SD: "19991231"}, "1999-12-31T12:00:00.000+01:00"),
    ({SD: "19991231", ST: "235959.0999"}, "1999-12-31T23:59:59.099+01:00"),
    ({SD: "19991231", ST: "235959", TZ: "+0200"}, "1999-12-31T22:59:59.000+01:00"),
    ({SD: "19991231", ST: "215959", TZ: "-0100"}, "1999-12-31T23:59:59.000+01:00"),
    ({SD: "19991231", ST: "235959", TZ: "0100"}, "1999-12-31T23:59:59.000+01:00"),
]


@pytest.mark.parametrize("dcm_fields,expected", TEST_GET_SESSION_TIMESTAMP_ARGS)
def test_meta_session_timestamp(dcm_fields, expected):
    dcm = create_dcm(**dcm_fields)
    assert dcm.get_meta().get("session.timestamp") == expected


sD, ADT = "SeriesDate", "AcquisitionDateTime"
TEST_GET_ACQUISITION_TIMESTAMP_ARGS = [
    ({}, None),
    # fallback to StudyDate
    ({SD: "19991231"}, "1999-12-31T12:00:00.000+01:00"),
    # prefer SeriesDate
    ({sD: "19991230", SD: "19991232"}, "1999-12-30T12:00:00.000+01:00"),
    ({ADT: "19991231235959"}, "1999-12-31T23:59:59.000+01:00"),
    ({ADT: "19991231235959", TZ: "+0200"}, "1999-12-31T22:59:59.000+01:00"),
    ({ADT: "19991231235959+0200"}, "1999-12-31T22:59:59.000+01:00"),
]


@pytest.mark.parametrize("dcm_fields,expected", TEST_GET_ACQUISITION_TIMESTAMP_ARGS)
def test_meta_acquisition_timestamp(dcm_fields, expected):
    dcm = create_dcm(**dcm_fields)
    assert dcm.get_meta().get("acquisition.timestamp") == expected


@pytest.mark.parametrize(
    "dcm_fields,expected",
    [
        # 1.1. SeriesNumber - SeriesDescription
        (
            {
                "SeriesNumber": 1,
                "SeriesDescription": "series desc",
                "ProtocolName": "prot name",
            },
            "1 - series desc",
        ),
        # 1.2. SeriesDescription
        (
            {"SeriesDescription": "series desc", "ProtocolName": "prot name"},
            "series desc",
        ),
        # 2.1 SeriesNumber - ProtocolName
        (
            {
                "SeriesNumber": 1,
                "ProtocolName": "prot name",
                "AcquisitionDateTime": "19991231235959",
            },
            "1 - prot name",
        ),
        # 2.2 ProtocolName
        (
            {"ProtocolName": "prot name", "AcquisitionDateTime": "19991231235959"},
            "prot name",
        ),
        # 3. acquisition timestamp
        # 3.1. AcquisitionDateTime
        (
            {"AcquisitionDateTime": "19991231235959", "AcquisitionDate": "20001231"},
            "1999-12-31T23-59-59",
        ),
        # 3.2. AcquisitionDate + AcquisitionTime
        (
            {"AcquisitionDate": "20001231", "AcquisitionTime": "235959"},
            "2000-12-31T23-59-59",
        ),
        # 3.2.1 AcquisitionDate
        ({"AcquisitionDate": "20001231"}, "2000-12-31T12-00-00"),
        # 3.2.2 AcquisitionTime
        (
            {
                "AcquisitionTime": "235959",
                "SeriesDate": "20011231",
                "SeriesTime": "235959",
            },
            "2001-12-31T23-59-59",
        ),
        # 3.3. SeriesDate + SeriesTime
        ({"SeriesDate": "20011231", "SeriesTime": "235959"}, "2001-12-31T23-59-59"),
        # 3.3.1 SeriesDate
        ({"SeriesDate": "20011231"}, "2001-12-31T12-00-00"),
        # 3.3.2 SeriesTime
        (
            {"SeriesTime": "235959", "StudyDate": "20021231", "StudyTime": "235959"},
            "2002-12-31T23-59-59",
        ),
        # 3.4. SeriesInstanceUID timestamp
        # 3.5. StudyDate + StudyTime
        ({"StudyDate": "20021231", "StudyTime": "235959"}, "2002-12-31T23-59-59"),
        # 3.5.1 StudyDate
        ({"StudyDate": "20021231"}, "2002-12-31T12-00-00"),
        # 3.5.2 StudyTime
        ({"StudyTime": "235959"}, "1.2"),
        # 3.6. StudyInstanceUID timestamp
        # 4. SeriesInstanceUID
        ({"SeriesNumber": 1}, "1 - 1.2"),
        # 5. (nothing from above)
        ({"SeriesInstanceUID": DICT_UNSET}, None),
    ],
)
def test_meta_acquisition_label(dcm_fields, expected):
    dcm = create_dcm(**dcm_fields)
    assert dcm.get_meta().get("acquisition.label") == expected


@pytest.mark.parametrize(
    "dcm_fields,expected",
    [
        # 1. StudyDescription
        ({"StudyDescription": "study desc"}, "study desc"),
        # 2. session timestamp
        # 2.1. StudyDate + StudyTime
        ({"StudyDate": "20021231", "StudyTime": "235959"}, "2002-12-31T23-59-59"),
        # 2.1.1 StudyDate
        ({"StudyDate": "20021231"}, "2002-12-31T12-00-00"),
        # 2.1.2 StudyTime
        ({"StudyTime": "235959"}, "1"),
        # 2.2. StudyInstanceUID timestamp
        # 2.3. SeriesDate + SeriesTime
        ({"SeriesDate": "20011231", "SeriesTime": "235959"}, "2001-12-31T23-59-59"),
        # 2.3.1 SeriesDate
        ({"SeriesDate": "20011231"}, "2001-12-31T12-00-00"),
        # 2.3.2 SeriesTime
        (
            {"SeriesTime": "235959", "StudyDate": "20021231", "StudyTime": "235959"},
            "2002-12-31T23-59-59",
        ),
        # 2.4. SeriesInstanceUID timestamp
        # 2.5. AcquisitionDateTime
        (
            {"AcquisitionDateTime": "19991231235959", "AcquisitionDate": "20001231"},
            "1999-12-31T23-59-59",
        ),
        # 2.6. AcquisitionDate + AcquisitionTime
        (
            {"AcquisitionDate": "20001231", "AcquisitionTime": "235959"},
            "2000-12-31T23-59-59",
        ),
        # 2.6.1 AcquisitionDate
        ({"AcquisitionDate": "20001231"}, "2000-12-31T12-00-00"),
        # 2.6.2 AcquisitionTime
        # 3. StudyInstanceUID
        ({}, "1"),
        # 4. (nothing from above)
        ({"StudyInstanceUID": DICT_UNSET}, None),
    ],
)
def test_meta_session_label(dcm_fields, expected):
    dcm = create_dcm(**dcm_fields)
    assert dcm.get_meta().get("session.label") == expected


@pytest.mark.parametrize(
    "dcm_fields,expected",
    [
        ({}, None),
        ({"PatientAge": "foo"}, None),
        ({"PatientAge": "1d"}, 86400),
        ({"PatientAge": "1D"}, 86400),
        ({"PatientAge": "1W"}, 604800),
        ({"PatientAge": "1M"}, 2592000),
        ({"PatientAge": "1Y"}, 31557600),
        ({"PatientAge": "1"}, 31557600),
        ({"PatientBirthDate": "foo"}, None),
        ({"PatientBirthDate": "19991231"}, None),
        ({"PatientBirthDate": "19991231", "AcquisitionDate": "foo"}, None),
        ({"PatientBirthDate": "19991231", "AcquisitionDate": "20000101"}, 86400),
        ({"PatientBirthDate": "20000101", "AcquisitionDate": "19991231"}, None),
    ],
)
def test_meta_session_age(dcm_fields, expected):
    dcm = create_dcm(**dcm_fields)
    assert dcm.get_meta().get("session.age") == expected


@pytest.mark.parametrize(
    "dcm_fields,expected",
    [
        ({}, (None, None)),
        ({"PatientName": "Doe^John"}, ("John", "Doe")),
        ({"PatientName": "John Doe"}, ("John", "Doe")),
        ({"PatientName": "Doe"}, (None, "Doe")),
    ],
)
def test_meta_patient_name(dcm_fields, expected):
    dcm = create_dcm(**dcm_fields)
    firstname = dcm.get_meta().get("subject.firstname")
    lastname = dcm.get_meta().get("subject.lastname")
    assert (firstname, lastname) == expected


def test_ge_physio_match():
    dcm = create_dcm()
    assert dcm.ge_physio_match is None
    dcm.Manufacturer = "GE MEDICAL SYSTEMS"
    assert dcm.ge_physio_match is None
    dcm.PulseSequenceName = "Test"
    dcm.AcquisitionDateTime = "20000101000000"
    dcm.AcquisitionDuration = 10
    dt = datetime(2000, 1, 1, 0, 0, 10, tzinfo=get_tzinfo())
    expected = {"psn": "test", "end": dt}
    assert dcm.ge_physio_match == expected
