import pydicom.config as pydicom_config
import pytest

from fw_file.dicom import get_config


def test_default_fixers():
    config = get_config()
    assert config.list_fixers() == [
        "tag_specific_fixer",
        "apply_dictionary_VR",
        "replace_backslash_in_VM1_str",
        "convert_exception_fixer",
        "LUT_descriptor_fixer",
    ]


def test_remove_fixer():
    config = get_config()
    config.remove_fixers(["tag_specific_fixer", "nonexistant_fixer"])
    assert config.list_fixers() == [
        "apply_dictionary_VR",
        "replace_backslash_in_VM1_str",
        "convert_exception_fixer",
        "LUT_descriptor_fixer",
    ]


def test_fn():
    pass


def test_add_fixer():
    config = get_config()
    config.remove_fixers("tag_specific_fixer")
    assert "tag_specific_fixer" not in config.list_fixers()
    assert "test_fn" not in config.list_fixers()
    config.add_fixers([test_fn, "fw_file.dicom.fixers.tag_specific_fixer"], "VR")
    assert "tag_specific_fixer" in config.list_fixers()
    assert "test_fn" in config.list_fixers()


def test_add_fixer_index():
    config = get_config()
    config.remove_fixers("tag_specific_fixer")
    assert "tag_specific_fixer" not in config.list_fixers()
    assert "test_fn" not in config.list_fixers()
    config.add_fixers([test_fn, "fw_file.dicom.fixers.tag_specific_fixer"], "VR", 1)
    assert config.list_fixers() == [
        "apply_dictionary_VR",
        "test_fn",
        "tag_specific_fixer",
        "replace_backslash_in_VM1_str",
        "convert_exception_fixer",
        "LUT_descriptor_fixer",
    ]


def test_add_fixer_fail():
    config = get_config()
    with pytest.raises(ValueError):
        config.add_fixers([test_fn], "not VR or value")


@pytest.mark.parametrize(
    "in_,err",
    [
        ("test_dicom_fixer", "Cannot import fixer 'test_dicom_fixer'"),
        (
            "fw_file.dicom.fixer.non_existant",
            "Cannot import fixer 'fw_file.dicom.fixer.non_existant'",
        ),
    ],
)
def test_add_fixer_error(in_, err):
    config = get_config()
    with pytest.raises(ValueError) as exc:
        config.add_fixers(in_, "value")
    assert err in exc.value.args[0]


def test_validation_mode_settings():
    config = get_config()
    assert config.reading_validation_mode == 1
    assert config.writing_validation_mode == 1
    assert pydicom_config.settings.reading_validation_mode == 1
    assert pydicom_config.settings.writing_validation_mode == 1

    config.reading_validation_mode = 2
    assert config.reading_validation_mode == 2
    assert pydicom_config.settings.reading_validation_mode == 2

    config.writing_validation_mode = 2
    assert config.writing_validation_mode == 2
    assert pydicom_config.settings.writing_validation_mode == 2
