from datetime import datetime, timezone

from fw_file import utils


def test_birthdate_to_age():
    bday = datetime(1970, 1, 1, 0, 0, 0, 0, tzinfo=timezone.utc)
    sess = datetime.now()
    assert utils.birthdate_to_age(bday, sess)


def test_birthdate_to_age_retnull():
    bday = datetime.now()
    sess = datetime(1970, 1, 1, 0, 0, 0, 0, tzinfo=timezone.utc)
    assert not utils.birthdate_to_age(bday, sess)
