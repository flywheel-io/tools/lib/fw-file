"""Tests for the dicom.utils module."""

import io
from datetime import datetime, timedelta, timezone

import pytest

from fw_file.dicom import utils
from fw_file.dicom.dicom import DICOM
from fw_file.dicom.testing import create_dcm, generate_dcm


@pytest.mark.parametrize(
    "sig, res",
    [
        (b"\x00" * 128 + b"DICM", True),
        (b"\x00" * 127 + b"DICM", False),
        (b"\x00" * 128 + b"DICOM", False),
        (b"" + b"DICM", False),
    ],
)
def test_sniff_dcm(sig, res):
    dcm = io.BytesIO()
    dcm.write(sig)
    assert utils.sniff_dcm(dcm) == res


def test_generate_uid():
    assert utils.generate_uid().startswith("2.16.840.1.114570.4.2.")
    assert utils.generate_uid() != utils.generate_uid()
    deterministic = utils.generate_uid(entropy_srcs=["foo"])
    assert deterministic.startswith("2.16.840.1.114570.4.2.")
    assert deterministic == utils.generate_uid(entropy_srcs=["foo"])
    assert deterministic != utils.generate_uid(entropy_srcs=["bar"])
    assert utils.generate_uid(prefix="1.2").startswith("1.2.")


def test_generate_dcm(tmp_path):
    generate_dcm(
        output_path=tmp_path,
        output_name="{sub_no}{ses_no}{acq_no}_{img_no}.dcm",
        studies=[{"StudyDescription": "foo"}],
        series=[{"SeriesDescription": "bar"}, {"SeriesDescription": "baz"}],
        images=3,
    )
    paths = sorted(tmp_path.iterdir(), key=lambda path: path.name)
    names = [path.name for path in paths]
    dcms = [DICOM(path) for path in paths]
    assert names == [
        "111_1.dcm",
        "111_2.dcm",
        "111_3.dcm",
        "112_1.dcm",
        "112_2.dcm",
        "112_3.dcm",
    ]
    assert all(dcm.StudyDescription == "foo" for dcm in dcms)
    assert all(dcm.SeriesDescription == "bar" for dcm in dcms[:3])
    assert all(dcm.SeriesDescription == "baz" for dcm in dcms[3:])


# FLYW-7639
def test_0001_01_01_date_conversion_bug():
    dcm = create_dcm(PatientBirthDate="00010101", AcquisitionDateTime="00010102")
    age = utils.get_session_age(dcm)
    assert age == (24 * 60 * 60)


@pytest.mark.parametrize(
    "tz",
    [
        timezone(offset=timedelta(hours=-6)),
        timezone.utc,
        timezone(offset=timedelta(hours=6)),
    ],
)
def test_datetime_str_offset_bounds(tz, mocker):
    # Without OverflowError handling in parse_datetime_str, offset -6 fails,
    # but utc and offset +6 should not hit the OverflowError.
    tzlocal = mocker.patch("tzlocal.get_localzone")
    tzlocal.return_value = tz
    res = utils.parse_datetime_str("00010101000000.000000+0000")

    assert res.date() == datetime(1, 1, 1).date()


def test_datetime_str_overflow_no_offset(mocker, caplog):
    get_dt = mocker.patch("fw_file.dicom.utils.get_datetime")
    get_dt.side_effect = OverflowError()

    res = utils.parse_datetime_str("00010101000000.000000")
    assert not res
    assert "Exception raised when calling parse_datetime_str" in caplog.text


def test_util_except_handler_get_session_age(mocker, caplog):
    dcm = create_dcm(PatientBirthDate="99999999", AcquisitionDateTime="99999999")
    parse_datetime_str_mock = mocker.patch("fw_file.dicom.utils.parse_datetime_str")
    parse_datetime_str_mock.side_effect = Exception("Something went wrong!")
    age = utils.get_session_age(dcm)

    assert not age
    assert "Exception raised when calling get_session_age" in caplog.text


def test_is_dcm():
    dcm = create_dcm()
    assert utils.is_dcm(dcm)


def test_is_dcm_fail(tmp_path):
    test_dcm = str(tmp_path / "test.dcm")
    with open(test_dcm, "w") as fp:
        fp.write("test")
    assert not utils.is_dcm(DICOM(test_dcm, force=True))


def test_is_dcm_with_unknown_tags():
    dcm = create_dcm()
    # Add OverlayRows repeater
    dcm.dataset.raw.add_new((0x6000, 0x0010), "US", 512)
    assert utils.is_dcm(dcm)


def test_is_dcm_sop_class_uid():
    dcm = create_dcm()
    del dcm["ImplementationClassUID"]
    assert utils.is_dcm(dcm)
