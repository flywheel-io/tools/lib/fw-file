"""Pytest conftest module."""

import os
import shutil
import tempfile
from pathlib import Path

import pytest
from pydicom.datadict import tag_for_keyword
from pydicom.dataelem import RawDataElement, convert_raw_data_element
from pydicom.tag import Tag

from fw_file.dicom.config import get_config

os.environ["TZ"] = "Europe/Budapest"
ASSETS_ROOT = Path(__file__).parent / "assets"
DICOM_ROOT = ASSETS_ROOT / "DICOM"


def load_raw(tag_or_kw, VR, length, value, dataset=None, encodings=None):
    # hardcode non-relevant value_tell, is_implicit_VR and is_little_endian
    # https://github.com/pydicom/pydicom/blob/v2.2.2/pydicom/dataelem.py#L727
    tag = Tag(tag_or_kw)
    if isinstance(tag_or_kw, str):
        tag_raw = tag_for_keyword(tag_or_kw)
        if tag_raw:
            tag = Tag(tag_raw)

    raw = RawDataElement(tag, VR, length, value, 1337, False, True)
    return convert_raw_data_element(raw, ds=dataset, encoding=encodings)


@pytest.fixture(autouse=True)
def config():
    get_config.cache_clear()
    return get_config()


@pytest.fixture
def dicom_file():
    """Returns path to dicom file in assets."""

    def get_dicom_file(folder, filename):
        fp, path = tempfile.mkstemp(suffix=".dcm")
        os.close(fp)

        src_path = os.path.join(DICOM_ROOT, folder, filename)
        shutil.copy(src_path, path)

        return path

    return get_dicom_file


@pytest.fixture(scope="session")
def assets():
    """Return asset directory."""
    return ASSETS_ROOT
