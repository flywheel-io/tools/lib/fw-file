import logging
from unittest.mock import MagicMock

import pytest
from pydicom import config as pydicom_config
from pydicom.dataelem import RawDataElement
from pydicom.dataset import Dataset, FileDataset
from pydicom.sequence import Sequence
from pydicom.tag import Tag

from fw_file.dicom import get_config
from fw_file.dicom.dicom import DSWrap
from fw_file.dicom.reader import ReadContext, no_fixes

from .conftest import load_raw


@pytest.fixture
def ctx():
    with ReadContext() as ctx_:
        yield ctx_


def test_no_fixes_cm():
    config = get_config()
    assert len(config.list_fixers())
    pydicom_config.replace_un_with_known_vr = True
    pydicom_config.convert_wrong_length_to_UN = True
    with no_fixes():
        config = get_config()
        assert len(config.list_fixers()) == 0
        assert not pydicom_config.replace_un_with_known_vr
        assert not pydicom_config.convert_wrong_length_to_UN
    config = get_config()
    assert len(config.list_fixers())
    assert pydicom_config.replace_un_with_known_vr
    assert pydicom_config.convert_wrong_length_to_UN


def test_read_context_original_attributes_exist():
    # Set up existing dataset
    ds = Dataset()
    modified_attrs = Dataset()
    modified_attrs.PatientName = "test"
    orig = Dataset()
    orig.ModifiedAttributesSequence = Sequence([modified_attrs])
    ds.OriginalAttributesSequence = Sequence([orig])
    # Create a read context and add one modified attribute
    context = ReadContext()

    # Trigger population of original attributes sequence
    context.modified_attrs.PatientName = "test1"
    context.update_orig_attrs(ds)
    assert len(ds.OriginalAttributesSequence) == 2
    my_mod = ds.OriginalAttributesSequence[1]
    assert my_mod.ModifiedAttributesSequence[0].PatientName == "test1"
    assert "SeriesDescription" not in my_mod.ModifiedAttributesSequence[0]
    assert my_mod.ModifyingSystem == "fw_file"

    # Trigger one more update
    context.modified_attrs.SeriesDescription = "tester"
    context.update_orig_attrs(ds)
    assert len(ds.OriginalAttributesSequence) == 2
    my_mod = ds.OriginalAttributesSequence[1]
    assert my_mod.ModifiedAttributesSequence[0].PatientName == "test1"
    assert my_mod.ModifiedAttributesSequence[0].SeriesDescription == "tester"
    assert my_mod.ModifyingSystem == "fw_file"


def test_read_context_original_attributes_dont_exist():
    context = ReadContext()
    context.modified_attrs.PatientName = "test1"
    ds = Dataset()
    context.update_orig_attrs(ds)
    assert len(ds.OriginalAttributesSequence) == 1
    my_mod = ds.OriginalAttributesSequence[0]
    assert my_mod.ModifiedAttributesSequence[0].PatientName == "test1"
    assert "PatientID" not in my_mod.ModifiedAttributesSequence[0]
    assert my_mod.ModifyingSystem == "fw_file"

    # Call update again
    context.update_orig_attrs(ds)
    context.modified_attrs.PatientID = "tester"
    assert len(ds.OriginalAttributesSequence) == 1
    my_mod = ds.OriginalAttributesSequence[0]
    assert my_mod.ModifiedAttributesSequence[0].PatientName == "test1"
    assert my_mod.ModifiedAttributesSequence[0].PatientID == "tester"
    assert my_mod.ModifyingSystem == "fw_file"


def test_data_elem_from_raw_sets_encodings(ctx):
    assert not ctx.encoding
    _ = load_raw(0x00280010, None, 2, (1024).to_bytes(2, "little"), encodings="utf-8")
    assert ctx.encoding == "utf-8"


def test_add_modified_element_falls_back_to_nonconforming(ctx):
    val = b"anonymized"
    raw = RawDataElement(Tag((0x0010, 0x0030)), "DA", len(val), val, 1337, False, True)
    ctx.add_modified_elem(raw)
    assert ctx.modified_attrs.PatientBirthDate == ""
    assert len(ctx.nonconforming_elems)
    nonconform = ctx.nonconforming_elems[0]
    assert nonconform.SelectorAttribute == Tag(0x0010, 0x0030)
    assert nonconform.NonconformingDataElementValue == b"anonymized"


@pytest.mark.parametrize("validation_mode", [1, 2])
def test_required_element_is_not_removed(validation_mode, config):
    config.reading_validation_mode = validation_mode
    with ReadContext() as ctx:
        # SeriesInstanceUID cannot be removed
        value = b"anonymized"
        _ = load_raw(0x00080016, None, len(value), value)
    req_tags = {0x00080016}
    # tag_dict.get.return_value = False
    dcm = Dataset()
    dcm.ensure_file_meta()
    ctx.process_tracker(req_tags, dcm)
    if validation_mode == 2:
        assert dcm[(0x0008, 0x0016)].value == "anonymized"
    else:
        assert (0x0008, 0x0016) not in dcm


def test_required_element_is_not_removed_failure():
    with ReadContext() as ctx:
        # WindowCenter is DS
        value = b"anonymized"
        _ = load_raw(0x00281050, None, len(value), value)
    req_tags = {0x00281050}
    dcm = Dataset()
    dcm.ensure_file_meta()
    with pytest.warns(UserWarning):
        ctx.process_tracker(req_tags, dcm)


def test_required_element_read_only(config):
    config.read_only = True
    with ReadContext() as ctx:
        # WindowCenter is DS
        value = b"anonymized"
        element = load_raw(0x00281050, None, len(value), value)
    req_tags = {0x00281050}
    dcm = Dataset()
    ctx.process_tracker(req_tags, dcm)
    # If read_only == True, VR is set to OB and otherwise kept as-is.
    assert not dcm
    assert element.value == b"anonymized"
    assert element.VR == "OB"


@pytest.mark.parametrize("validation_mode", [1, 2])
@pytest.mark.parametrize(
    "req_tags",
    [
        # Both an empty set and a set that doesn't contain the tag of interest
        # should have same result since group 0002 is not removed either way
        ({}),
        ({0x00010001}),
    ],
)
def test_group_0002_is_not_removed(req_tags, validation_mode, config):
    config.reading_validation_mode = validation_mode
    with ReadContext() as ctx:
        # SeriesInstanceUID cannot be removed
        value = b"anonymized"
        _ = load_raw(0x00020002, None, len(value), value)
    dcm = Dataset()
    ctx.process_tracker(req_tags, dcm)
    if validation_mode == 2:
        assert dcm[(0x0002, 0x0002)].value == "anonymized"
    else:
        assert (0x0002, 0x0002) not in dcm


def test_dataset_read_context_injected():
    ds = Dataset()
    ds.PatientName = "test"
    read_context = MagicMock()
    res = DSWrap(ds, read_context)
    assert res.PatientName == "test"
    read_context.__enter__.assert_called_once()
    read_context.__exit__.assert_called_once()


def test_file_dataset_read_context_injected(tmp_path):
    ds = FileDataset(tmp_path, Dataset(), None, None, False, False)
    ds.PatientName = "test"
    read_context = MagicMock()
    res = DSWrap(ds, read_context)
    assert res.PatientName == "test"
    read_context.__enter__.assert_called_once()
    read_context.__exit__.assert_called_once()
    assert res.raw.filename == str(tmp_path)
    assert res.raw.is_implicit_VR is False
    assert res.raw.is_little_endian is False


def test_add_modified_element_from_tuple(ctx):
    elem = (0x00100010, "PN", "original^value")
    ctx.add_modified_elem(elem)
    assert len(ctx.modified_attrs) == 1
    assert ctx.modified_attrs[(0x0010, 0x0010)].value == "original^value"


def test_add_modified_element_from_tuple_invalid(ctx, caplog):
    # "male" is not valid for a Code-String VR
    elem = (0x00100040, "CS", "male")
    with caplog.at_level(logging.DEBUG):
        ctx.add_modified_elem(elem)
    assert len(caplog.record_tuples) == 1
    assert "value 'male' for tag 00100040 invalid" in caplog.record_tuples[0][2]
    assert len(ctx.nonconforming_elems) == 1
    assert ctx.nonconforming_elems[0][(0x0400, 0x0552)].value == b"male"


def test_add_modified_element_overflow_error(ctx, caplog):
    # (0008, 2130) = Event Elapsed Time
    # Catches OverflowError when "DS" element value is <= 16 chars
    elem = (0x00082130, "DS", "123456789.123456789")
    with caplog.at_level(logging.DEBUG):
        ctx.add_modified_elem(elem)
    assert len(caplog.record_tuples) == 1
    assert (
        "value '123456789.123456789' for tag 00082130 invalid"
        in caplog.record_tuples[0][2]
    )
    assert len(ctx.nonconforming_elems) == 1
    assert ctx.nonconforming_elems[0][(0x0400, 0x0552)].value == b"123456789.123456789"
