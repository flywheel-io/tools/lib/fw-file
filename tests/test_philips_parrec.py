"""Tests for the Philips PARREC format."""

import io
import pathlib
import re
from decimal import Decimal

import pytest

from fw_file.philips import PARFile

from .conftest import ASSETS_ROOT

PARREC_ROOT = ASSETS_ROOT / "parrec"
TEST_PAR_FILE = PARREC_ROOT / "rf_wipsurvey_v42.par"
TEST_PAR_ZIP_FILE = PARREC_ROOT / "phantom_truncated.parrec.zip"


def test_get_field():
    par = PARFile(TEST_PAR_FILE)
    assert par.patient_name == "rf"
    assert par.examination_name == "rfr test2"
    assert par.angulation_midslice == [
        Decimal("0.00"),
        Decimal("0.00"),
        Decimal("0.00"),
    ]
    assert par.scan_duration == Decimal("23.11")
    assert par["Scan Duration [sec]"] == Decimal("23.11")
    assert par["Flow compensation <0=no 1=yes> ?"] == 0


def test_get_invalid_key():
    par = PARFile(TEST_PAR_FILE)
    with pytest.raises(KeyError):
        par["[??]"]


def test_set_invalid_key():
    par = PARFile(TEST_PAR_FILE)
    with pytest.raises(KeyError):
        par.attr = "foo"


def test_get_keys():
    par = PARFile(TEST_PAR_FILE)
    assert "patient_name" in list(par.keys())


def test_get_len():
    par = PARFile(TEST_PAR_FILE)
    assert len(par) == 162


def test_save_par_file(tmp_path):
    par = PARFile(TEST_PAR_FILE)
    par.patient_name = "anon"
    par.examination_name = None
    del par["Acquisition nr"]
    filepath = tmp_path / "test.par"
    par.save(filepath)
    par = PARFile(filepath)
    assert par.patient_name == "anon"
    assert par.examination_name is None
    assert par.get("Acquisition nr") is None
    # lines are identical except the two modified ones
    orig = pathlib.Path(TEST_PAR_FILE).read_text(encoding="utf-8")
    expected = re.sub(r"(Patient name\s+:\s+).*\n", r"\1anon\n", orig)
    expected = re.sub(r"(Examination name\s+:\s+).*\n", r"\1\n", expected)
    expected = re.sub(r"\n.*Acquisition nr.*\n", "\n", expected)
    assert pathlib.Path(filepath).read_text(encoding="utf-8") == expected


def test_save_raises_on_invalid_file():
    par = PARFile(io.BytesIO(b"foo"))
    with pytest.raises(ValueError):
        par.save()


def test_meta():
    par = PARFile(TEST_PAR_FILE)
    assert par.get_meta() == {
        "subject.label": "rf",
        "session.label": "rfr test2",
        "acquisition.label": "WIP SURVEY_1_1",
        "acquisition.timestamp": "2012-10-12T13:32:44.000+02:00",
        "acquisition.timezone": "Europe/Budapest",
        "file.name": "rf_wipsurvey_v42.par",
        "file.type": "parrec",
    }


PROTO, ACQNO, RECON = "protocol_name", "acquisition_nr", "reconstruction_nr"


@pytest.mark.parametrize(
    "fields,expected",
    [
        ({PROTO: None, ACQNO: None, RECON: None}, None),
        ({PROTO: None, ACQNO: None, RECON: 2}, "2"),
        ({PROTO: None, ACQNO: 1, RECON: 2}, "1_2"),
        ({PROTO: "prt", ACQNO: 1, RECON: 2}, "prt_1_2"),
        ({PROTO: "prt", ACQNO: None, RECON: 2}, "prt_2"),
    ],
)
def test_meta_acq_label(fields, expected):
    par = PARFile(TEST_PAR_FILE)
    par.update(fields)
    assert par.get_meta().get("acquisition.label") == expected


@pytest.mark.parametrize("exam_dt", [None, "", "foo"])
def test_meta_acq_timestamp_invalid_format(exam_dt):
    par = PARFile(TEST_PAR_FILE)
    par.examination_date_time = exam_dt
    assert "acquisition.timestamp" not in par.get_meta()


def test_parfile_from_zip():
    par = PARFile.from_zip(TEST_PAR_ZIP_FILE)
    meta = par.get_meta()
    assert par.patient_name == "phantom"
    assert meta == {
        "subject.label": "phantom",
        "session.label": "Konvertertest",
        "acquisition.label": "EPI_asc CLEAR_2_1",
        "acquisition.timestamp": "2014-02-14T09:00:57.000+01:00",
        "acquisition.timezone": "Europe/Budapest",
        "file.name": "phantom_truncated.PAR",
        "file.type": "parrec",
    }


def test_parfile_from_zip_no_par():
    with pytest.raises(FileNotFoundError):
        PARFile.from_zip("tests/assets/eeg/eeglab.zip")
