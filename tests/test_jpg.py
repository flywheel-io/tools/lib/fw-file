"""Tests for JPG file format."""

import io
import os

import piexif
import pytest
from PIL import Image

from fw_file.jpg import JPG

ZEROTH = {
    piexif.ImageIFD.Make: "Canon",
    piexif.ImageIFD.XResolution: (96, 1),
    piexif.ImageIFD.YResolution: (96, 1),
    piexif.ImageIFD.Software: "piexif",
}

EXIF_IFD = {
    piexif.ExifIFD.DateTimeOriginal: "2099:09:29 10:10:10",
    piexif.ExifIFD.LensMake: "LensMake",
    piexif.ExifIFD.Sharpness: 65535,
    piexif.ExifIFD.LensSpecification: ((1, 1), (1, 1), (1, 1), (1, 1)),
}

GPS = {
    piexif.GPSIFD.GPSVersionID: (2, 0, 0, 0),
    piexif.GPSIFD.GPSAltitudeRef: 1,
    piexif.GPSIFD.GPSDateStamp: "1999:99:99 99:99:99",
}

FIRST = {
    piexif.ImageIFD.Make: "Canon",
    piexif.ImageIFD.XResolution: (40, 1),
    piexif.ImageIFD.YResolution: (40, 1),
    piexif.ImageIFD.Software: "piexif",
}

EXIF_DICT = {"0th": ZEROTH, "Exif": EXIF_IFD, "GPS": GPS, "1st": FIRST}


@pytest.fixture
def jpg():
    tmb = io.BytesIO()
    tmb_img = Image.new("RGB", (1, 1), "green")
    tmb_img.save(tmb, format="JPEG")
    thumb = {"thumbnail": tmb.getvalue()}
    exif_bytes = piexif.dump({**EXIF_DICT, **thumb})
    file = io.BytesIO()
    im = Image.new("RGB", (500, 500), "green")
    im.save(file, format="JPEG", exif=exif_bytes)
    return JPG(file)


def test_jpeg_default_meta(jpg, tmp_path):
    assert jpg.get_meta() == {"file.type": "image"}

    with open(tmp_path / "test.jpg", "wb") as fp:
        jpg.save(fp)

    jpg = JPG(f"{tmp_path}/test.jpg")
    assert jpg.get_meta() == {
        "file.name": "test.jpg",
        "file.type": "image",
    }


def test_jpg_save(jpg, tmp_path):
    jpg.save(tmp_path / "test.jpg")
    assert os.stat(tmp_path / "test.jpg").st_size > 0


def test_jpg_save_open_file(jpg, tmp_path):
    path = tmp_path / "test.jpg"
    with open(path, "wb") as fp:
        jpg.save(fp)
    assert path.stat().st_size


def test_jpg_get(jpg):
    assert jpg["1st", "XResolution"] == (40, 1)
    assert jpg["1ST", "xresolution"] == (40, 1)
    # resturns first find if no IFD specified, in this case 0th
    assert jpg.xresolution == (96, 1)
    # get raw IFD dict
    assert jpg["0th"][271] == b"Canon"


def test_jpg_get_wrong_key(jpg):
    with pytest.raises(KeyError):
        _ = jpg["WrongKey"]
    assert jpg.get("WrongKey") is None


def test_jpg_get_str_key_not_set(jpg):
    with pytest.raises(KeyError):
        _ = jpg["ExposureTime"]
    assert jpg.get("ExposureTime") is None


def test_jpg_set(jpg):
    assert jpg["1st", "XResolution"] == (40, 1)
    jpg["1st", "XResolution"] = (50, 1)
    assert jpg["1st", "XResolution"] == (50, 1)
    jpg["1ST", "Xresolution"] = (55, 1)
    assert jpg["1st", "XResolution"] == (55, 1)
    # update raw IFD dict
    jpg["0th"][271] = b"Nikon"
    assert jpg["0th", "Make"] == b"Nikon"
    # update whole IFD
    jpg.gps = {}
    assert jpg.gps == {}


def test_jpg_set_by_tag(jpg):
    with pytest.raises(KeyError, match="Ambiguous key"):
        jpg["XResolution"] = (50, 1)
    jpg["Sharpness"] = 61000
    assert jpg["Exif", "Sharpness"] == 61000
    jpg["gpsAltituderef"] = 2
    assert jpg["GPS", "GPSAltitudeRef"] == 2


def test_jpg_set_invalid_key(jpg):
    with pytest.raises(KeyError):
        jpg["WrongKey"] = (50, 1)


def test_jpg_del(jpg):
    del jpg["1st", "XResolution"]
    assert jpg.get(("1st", "XResolution")) is None
    assert jpg["0th", "XResolution"]
    assert jpg["XResolution"]

    del jpg.YResolution
    assert jpg.get("YResolution") is None

    del jpg.gps
    assert jpg.get("gps") is None


def test_jpg_del_invalid_key(jpg):
    with pytest.raises(KeyError):
        del jpg["WrongKey"]


def test_jpg_del_key_not_set(jpg):
    with pytest.raises(KeyError):
        del jpg["FillOrder"]


def test_jpg_save_modifications(jpg, tmp_path):
    jpg["Sharpness"] = 61000
    jpg["gpsAltituderef"] = 2
    del jpg["Make"]

    path = tmp_path / "test.jpg"
    jpg.save(path)
    jpg = JPG(path)
    assert jpg["Sharpness"] == 61000
    assert jpg["gpsAltituderef"] == 2
    assert jpg.get("Make") is None
