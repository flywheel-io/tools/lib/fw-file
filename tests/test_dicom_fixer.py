"""Test RawDataElement fixers and tracking."""

from unittest.mock import MagicMock

import pytest
from pydicom import data, dcmread
from pydicom.charset import convert_encodings
from pydicom.dataset import Dataset
from pydicom.tag import Tag
from pydicom.valuerep import PersonName

from fw_file.dicom import DICOM
from fw_file.dicom.reader import ReadContext

from .conftest import load_raw


@pytest.fixture
def ctx():
    with ReadContext() as ctx_:
        yield ctx_


@pytest.fixture()
def evt(ctx):
    yield lambda: [str(evt) for rde in ctx.data_elements for evt in rde.events]


def test_add_apply_dictionary_VR(config):
    assert "apply_dictionary_VR" in config.list_fixers()
    config.remove_fixers("apply_dictionary_VR")
    assert "apply_dictionary_VR" not in config.list_fixers()


def test_apply_dictionary_VR_on(ctx):
    de = load_raw(0x00280010, None, 2, (1024).to_bytes(2, "little"))
    assert de.value == 1024
    assert de.VR == "US"
    res = ctx.data_elements[0].export()
    assert res["original"].VR is None
    assert res["final"].VR == "US"
    assert str(res["events"][0]) == "Replace VR: None -> US"


def test_apply_dictionary_VR_unknown_tag_sets_to_UN(evt):
    de = load_raw(0x00700043, None, 2, (1024).to_bytes(2, "little"))
    assert de.value.hex() == "0004"
    assert de.VR == "UN"
    assert evt() == ["Replace VR: None -> UN"]


def test_apply_dictionary_VR_unknown_group_length_tag_sets_to_UL(evt):
    de = load_raw(0x00100000, None, 4, (1024).to_bytes(4, "little"))
    assert de.value == 1024
    assert de.VR == "UL"
    assert evt() == ["Replace VR: None -> UL"]


def test_apply_dictionary_VR_on_private_not_in_dict(evt):
    de = load_raw(0x37111003, None, 2, (10).to_bytes(2, "little"))
    assert de.value.hex() == "0a00"
    assert de.VR == "UN"
    assert evt() == ["Replace VR: None -> UN"]


def test_apply_dictionary_VR_off(config):
    config.remove_fixers("apply_dictionary_VR")
    de = load_raw(0x00280010, "UN", 2, (1024).to_bytes(2, "little"))
    assert de.value == b"\x00\x04"
    assert de.VR == "US"
    # NOTE: Turns out pydicom does dictionary_VR lookup with a "UN" VR in the
    # actually DataElement constructor...


def test_apply_dictionary_VR_from_UN(evt):
    de = load_raw(0x00280010, "UN", 2, (1024).to_bytes(2, "little"))
    assert de.value == 1024
    assert de.VR == "US"
    assert evt() == ["Replace VR: UN -> US"]


def test_apply_dictionary_VR_on_private_in_dict(evt):
    ds = Dataset()
    pc = load_raw(0x37110010, "UN", 25, b"A.L.I. Technologies, Inc.")
    ds[(0x3711, 0x0010)] = pc
    de = load_raw(0x37111003, "UN", 2, (10).to_bytes(2, "little"), dataset=ds)
    assert de.value == 10
    assert de.VR == "US"
    assert evt() == ["Replace VR: UN -> LO", "Replace VR: UN -> US"]


def test_replace_UN_with_known_VR_on_private_not_in_dict(evt):
    de = load_raw(0x37111003, "UN", 2, (10).to_bytes(2, "little"))
    assert de.VR == "UN"
    assert evt() == []


def test_replace_backslash_in_VM1_str_off(evt, config):
    config.remove_fixers("replace_backslash_in_VM1_str")
    de = load_raw(0x0008103E, "LO", 10, b"Lung\\Axial")
    assert de.value == ["Lung", "Axial"]
    assert evt() == []


def test_replace_backslash_in_VM1_str_on(evt):
    de = load_raw(0x0008103E, "LO", 10, b"Lung\\Axial")
    assert de.value == "Lung_Axial"
    assert evt() == [r"Replace value: b'Lung\\Axial' -> b'Lung_Axial'"]


@pytest.mark.parametrize(
    "kw,VR,value,fixed,final,final_VR,validation_mode",
    [
        # NOTE: In pydicom, all multivalues automatically have `\x00` stripped
        # from the end
        # When representation fix is not needed,
        # both validation_modes return the same final
        # As of pydicom 2.4.1 reading validation mode of 1 no longer still
        # raises an error so these are not fixed unless reading_validation is 2
        ("FocalDistance", "IS", b"1.2\\5.6\x00", b"1\\5", [1, 5], "IS", 2),
        ("FocalDistance", "IS", b"1.2\x00", b"1", 1, "IS", 2),
        # Remove trailing \x00 since that is automatically stripped, but _not_
        # in the tracker.
        ("FocalDistance", "IS", b"1.2\\5.6", b"1.2\\5.6", [1.2, 5.6], "IS", 1),
        ("FocalDistance", "IS", b"1.2", b"1.2", 1.2, "IS", 1),
        ("FocalDistance", "IS", b"1.2\\5.6a\x00", b"1\\5", [1, 5], "IS", 1),
        ("FocalDistance", "DS", b"1.2\\5.6a\x00", b"1\\5", [1, 5], "IS", 1),
        ("FocalDistance", "IS", b"1.2\\5.6\x00", b"1\\5", [1, 5], "IS", 2),
        ("FocalDistance", "IS", b"1.2\x00", b"1", 1, "IS", 2),
        ("FocalDistance", "IS", b"1.2\\5.6a\x00", b"1\\5", [1, 5], "IS", 2),
        ("FocalDistance", "DS", b"1.2\\5.6a\x00", b"1\\5", [1, 5], "IS", 2),
        ("EventElapsedTimes", "DS", b"1.2\\5.6a\x00", b"1.2\\5.6", [1.2, 5.6], "DS", 1),
        ("EventElapsedTimes", "DS", b"1.2\\5.6a\x00", b"1.2\\5.6", [1.2, 5.6], "DS", 2),
        # For representation fix, when validation_mode == 1, values are decoded but otherwise the same
        # >16 characters for representation fix
        (
            "ImagePositionPatient",
            "DS",
            b"0.0\\0.0\\-4.2345123456712e-2",
            b"0.0\\0.0\\-4.2345123456712e-2",
            [0.0, 0.0, -4.2345123456712e-2],
            "DS",
            1,
        ),
        # and when validation_mode == 2, values are decoded AND scientific representation is removed
        (
            "ImagePositionPatient",
            "DS",
            b"0.0\\0.0\\-4.2345123456712e-2",
            b"0.0\\0.0\\-0.0423451234567",
            [0, 0, -0.0423451234567],
            "DS",
            2,
        ),
    ],
)
def test_fix_number_string(
    evt, kw, VR, value, fixed, final, final_VR, validation_mode, config
):
    config.reading_validation_mode = validation_mode
    de = load_raw(kw, VR, len(value), value)
    assert de.value == final
    assert de.VR == final_VR
    out = evt()

    if value == fixed:
        # ImagePositionPatient validation_mode=1
        assert out == []
    elif fixed:
        assert f"Replace length: {len(value)} -> {len(fixed)}" in out
        assert rf"Replace value: {value} -> {fixed}" in out
    else:
        assert f"Replace length: {len(value)} -> 0" in out
        assert rf"Replace value: {value} -> " in out


def test_fix_number_string_fails(evt):
    value = b"anonymized"
    de = load_raw("FocalDistance", "DS", len(value), value)
    assert de.value is None
    assert de.VR == "IS"
    assert evt() == [
        "Replace VR: DS -> IS",
        "Replace value: b'anonymized' -> ",
        "Replace length: 10 -> 0",
    ]


def test_fix_number_string_read_only(evt, config):
    config.read_only = True
    value = b"anonymized"
    de = load_raw("FocalDistance", "DS", len(value), value)
    assert de.value == b"anonymized"
    assert de.VR == "OB"
    assert evt() == [
        "Replace VR: DS -> IS",
        "Replace VR: IS -> OB",
    ]


@pytest.mark.parametrize(
    "kw,VR,value,evts,final,enc,validation_mode",
    [
        # Can't decode this fully with default character set, so remove non-printable
        # NOTE: `\xb(B` is an escape sequence, so it's removed from the string
        # First set, decoded and escape sequence rm,
        # with `\x1b` changed to `_` when validation_mode == 2.
        (
            "PatientName",
            "PN",
            b"\x1b$BMz%j%7\x1b(B",
            0,
            "\x1b$BMz%j%7",
            b"ISO_IR 100",
            1,
        ),
        (
            "PatientName",
            "PN",
            b"\x1b$BMz%j%7\x1b(B",
            2,
            "_$BMz%j%7",
            b"ISO_IR 100",
            2,
        ),
        # Second set, this time without "ISO_IR" specified as encoding,
        # expected output is the same as when we do specifiy "ISO_IR" as encoding.
        ("PatientName", "PN", b"\x1b$BMz%j%7\x1b(B", 0, "\x1b$BMz%j%7", b"", 1),
        ("PatientName", "PN", b"\x1b$BMz%j%7\x1b(B", 2, "_$BMz%j%7", b"", 2),
        # Third set, specified encoding takes over no matter the validation mode.
        (
            "PatientName",
            "PN",
            b"\x1b$BMz%j%7\x1b(B",
            0,
            "履リシ",
            b"ISO_IR 100\\ISO 2022 IR 87",
            1,
        ),
        (
            "PatientName",
            "PN",
            b"\x1b$BMz%j%7\x1b(B",
            0,
            "履リシ",
            b"ISO_IR 100\\ISO 2022 IR 87",
            2,
        ),
        # Fourth set, when validation_mode == 1, \xc3\x93 is decoded into Ã and outputted
        # But when validation_mode == 2, because this doesn't comply to DICOM standard, value is removed.
        (
            (0x0033, 0x1004),
            "CS",
            b"TC T\xc3\x93RAX, TC ABDOMEN, TC PELVIS",
            0,
            "TC TÃRAX, TC ABDOMEN, TC PELVIS",
            b"",
            1,
        ),
        (
            (0x0033, 0x1004),
            "CS",
            b"TC T\xc3\x93RAX, TC ABDOMEN, TC PELVIS",
            2,
            "",
            b"",
            2,
        ),
    ],
)
def test_fix_invalid_char_text_string(
    evt, kw, VR, value, evts, final, enc, validation_mode, config
):
    config.reading_validation_mode = validation_mode
    encodings = convert_encodings(load_raw(0x00080005, "CS", len(enc), enc).value)
    de = load_raw(kw, VR, len(value), value, encodings=encodings)
    assert de.value == final
    # 2 for each change, 1 for length change, 1 for value change
    assert len(evt()) == evts


def test_fix_invalid_char_text_string_read_only(evt, config):
    config.read_only = True
    config.reading_validation_mode = 2
    kw = (0x0033, 0x1004)
    VR = "CS"
    value = b"TC T\xc3\x93RAX, TC ABDOMEN, TC PELVIS"
    enc = b""
    encodings = convert_encodings(load_raw(0x00080005, "CS", len(enc), enc).value)
    de = load_raw(kw, VR, len(value), value, encodings=encodings)
    # When read_only == True, value should remain as-is,
    # and VR should be changed to OB.
    assert de.value == value
    assert evt() == ["Replace VR: CS -> OB"]


@pytest.mark.parametrize(
    "tag,VR,value,final,validation_mode",
    [
        # When validation_mode == 1, values are simply decoded.
        (0x00080020, "DA", b"2022-02-04", "2022-02-04", 1),
        (0x00080030, "TM", b"10:30:01pm", "10:30:01pm", 1),
        (0x0008002A, "DT", b"2022-02-04 10:30:01pm", "2022-02-04 10:30:01pm", 1),
        # When validation_mode == 2, values are converted to DICOM standard.
        (0x00080020, "DA", b"2022-02-04", "20220204", 2),
        (0x00080030, "TM", b"10:30:01pm", "223001.0", 2),
        (0x0008002A, "DT", b"2022-02-04 10:30:01pm", "20220204223001.0", 2),
        # (0x0008002A, "DT", b"cannot be parsed", ""),
        # (0x0008002A, "CS", b"asdf-0=x/?`~", ""),
    ],
)
def test_invalid_value_for_VR_replaces_date_vrs(
    evt, tag, VR, value, final, validation_mode, config
):
    config.reading_validation_mode = validation_mode
    de = load_raw(tag, VR, len(value), value)
    assert de.value == final
    if validation_mode == 2:
        assert evt() == [
            f"Replace length: {len(value)} -> {len(final)}",
            rf"Replace value: {value} -> b'{final}'",
        ]


@pytest.mark.parametrize(
    "kw,VR,value,final,validation_mode",
    [
        # When validation_mode == 1, values are simply decoded.
        (
            "StudyDate",
            "DA",
            b"2022-02-04",
            "2022-02-04",
            1,
        ),
        (
            "StudyTime",
            "TM",
            b"10:30:01pm",
            "10:30:01pm",
            1,
        ),
        (
            "AcquisitionDateTime",
            "DT",
            b"2022-02-04 10:30:01pm",
            "2022-02-04 10:30:01pm",
            1,
        ),
        # When validation_mode == 2, values are converted to DICOM standard.
        (
            "StudyDate",
            "DA",
            b"2022-02-04",
            "20220204",
            2,
        ),
        (
            "StudyTime",
            "TM",
            b"10:30:01pm",
            "223001.0",
            2,
        ),
        (
            "AcquisitionDateTime",
            "DT",
            b"2022-02-04 10:30:01pm",
            "20220204223001.0",
            2,
        ),
    ],
)
def test_fix_invalid_VR_error_date_vrs(
    evt, kw, VR, value, final, validation_mode, config
):
    config.reading_validation_mode = validation_mode
    de = load_raw(kw, VR, len(value), value)
    assert de.value == final
    if validation_mode == 2:
        assert evt() == [
            f"Replace length: {len(value)} -> {len(final)}",
            f"Replace value: {value} -> {final.encode(encoding='ascii')}",
        ]


@pytest.mark.parametrize(
    "kw,VR,value,final,validation_mode",
    [
        # When validation_mode == 1, values are simply decoded
        ("PatientAge", "AS", b"1Y", "1Y", 1),
        ("PatientAge", "AS", b"01y", "01y", 1),
        ("PatientAge", "AS", b"01", "01", 1),
        ("PatientAge", "AS", b"00001", "00001", 1),
        ("PatientAge", "AS", b"00001y", "00001y", 1),
        ("PatientSex", "CS", b"m", "m", 1),
        # When validation_mode == 2, values are changed to match DICOM standard
        ("PatientAge", "AS", b"1Y", "001Y", 2),
        ("PatientAge", "AS", b"01y", "001Y", 2),
        ("PatientAge", "AS", b"01", "001Y", 2),
        ("PatientAge", "AS", b"00001", "001Y", 2),
        ("PatientAge", "AS", b"00001y", "001Y", 2),
        ("PatientSex", "CS", b"m", "M", 2),
    ],
)
def test_fix_invalid_VR_AS_CS_success(
    evt, kw, VR, value, final, validation_mode, config
):
    config.reading_validation_mode = validation_mode
    # Left pad (AS only), and convert to uppercase
    de = load_raw(kw, VR, len(value), value)
    assert de.value == final
    if validation_mode == 2:
        exp = [f"Replace value: {value} -> {final.encode(encoding='ascii')}"]
        if len(value) != len(final):
            exp.insert(0, f"Replace length: {len(value)} -> {len(final)}")
        assert evt() == exp


@pytest.mark.parametrize(
    "VR,value,validation_mode",
    [
        # If validation_mode == 1, values are simply decoded.
        ("AS", b"1234", 1),
        ("AS", b"123YM", 1),
        ("AS", b"Y123", 1),
        # If validation_mode == 2, values are removed.
        ("AS", b"1234", 2),  # Can't convert without losing information
        ("AS", b"123YM", 2),  # Can't convert without losing information
        ("AS", b"Y123", 2),  # TODO: Do we want to support this?
    ],
)
def test_fix_invalid_VR_AS_CS_fail(evt, VR, value, validation_mode, config):
    config.reading_validation_mode = validation_mode
    # Left pad (AS only), and convert to uppercase
    de = load_raw("PatientAge", VR, len(value), value)
    if validation_mode == 2:
        assert de.value == ""
        assert evt() == [
            f"Replace value: {value} -> ",
            f"Replace length: {len(value)} -> 0",
        ]
    else:
        assert de.value == value.decode()


@pytest.mark.parametrize(
    "VR,value",
    [
        ("AS", b"1234"),  # Can't convert without losing information
        ("AS", b"123YM"),  # Can't convert without losing information
        ("AS", b"Y123"),  # TODO: Do we want to support this?
    ],
)
def test_fix_invalid_VR_AS_CS_read_only(evt, VR, value, config):
    config.read_only = True
    config.reading_validation_mode = 2
    de = load_raw("PatientAge", VR, len(value), value)
    # When read_only = True, because the values cannot be converted
    # without losing information, values are retained and VR is
    # changed to OB.
    assert de.value == value
    assert evt() == ["Replace VR: AS -> OB"]


@pytest.mark.parametrize(
    "value,final,validation_mode",
    [
        # If validation_mode == 1, values are simply decoded.
        (b"1.02.03.04", "1.02.03.04", 1),
        (
            b"1.02.03.04a",
            "1.02.03.04a",
            1,
        ),
        # If validation_mode == 2, values are changed per DICOM standard.
        (b"1.02.03.04", "1.2.3.4", 2),
        (
            b"1.02.03.04a",
            "2.16.840.1.114570.4.2.865945595092006009173544939981818662720357",
            2,
        ),
    ],
)
def test_fix_ui_VRs_deterministic(evt, value, final, validation_mode, config):
    config.reading_validation_mode = validation_mode
    de = load_raw("SeriesInstanceUID", "UI", len(value), value)
    assert de.value == final
    if validation_mode == 2:
        assert evt() == [
            f"Replace length: {len(value)} -> {len(final)}",
            f"Replace value: {value} -> {final.encode(encoding='ascii')}",
        ]


@pytest.mark.parametrize(
    "value,validation_mode",
    [
        # If validation_mode == 1, the value is simply decoded.
        (b"A", 1),
        (b"Anonymized", 1),
        # If validation_mode == 2, the value is replaced with generated value.
        (b"A", 2),
        (b"Anonymized", 2),
    ],
)
def test_fix_ui_VRs_generates_new(evt, value, validation_mode, config):
    config.reading_validation_mode = validation_mode
    de = load_raw("SeriesInstanceUID", "UI", len(value), value)
    if validation_mode == 2:
        assert de.value != value.decode()
        evts = evt()
        # One for value, one for length
        assert len(evts) == 2
        assert evt()[1].startswith(f"Replace value: {value} -> ")
    else:
        assert de.value == value.decode()


@pytest.mark.parametrize("validation_mode", [1, 2])
@pytest.mark.parametrize(
    "kw,fix",
    [
        # When validation_mode == 1, de.value is simply the decoded value.
        # When validation_mode == 2, this chunk is expected to be fixed to 1.2.3.4
        ("SeriesInstanceUID", True),
        ("StudyInstanceUID", True),
        ("ReferencedSOPInstanceUID", True),
        ("SOPInstanceUID", True),
        ("MediaStorageSOPInstanceUID", True),
        # When validation_mode == 2, this chunk is expected to be removed.
        ("SOPClassUID", False),
        ("TransferSyntaxUID", False),
        ("ReferencedSOPClassUID", False),
        ("MediaStorageSOPClassUID", False),
    ],
)
def test_fix_ui_VRs_scope(kw, fix, validation_mode, ctx, config):
    config.reading_validation_mode = validation_mode
    value = b"1.02.03.04"
    de = load_raw(kw, "UI", len(value), value)
    if validation_mode == 1:
        assert de.value == "1.02.03.04"
    elif fix:
        assert de.value == "1.2.3.4"
    else:
        assert de.value == ""


@pytest.mark.parametrize(
    "kw",
    [
        ("SOPClassUID"),
        ("TransferSyntaxUID"),
        ("ReferencedSOPClassUID"),
        ("MediaStorageSOPClassUID"),
    ],
)
def test_fix_ui_VRs_scope_read_only(kw, config):
    config.read_only = True
    config.reading_validation_mode = 2
    value = b"1.02.03.04"
    de = load_raw(kw, "UI", len(value), value)
    # When validation_mode == 2, these values would be removed
    # when not read_only. When read_only, value remains as-is
    # and VR changes to OB.
    assert de.value == value
    assert de.VR == "OB"


@pytest.mark.parametrize(
    "tag,VR,value,final_VR",
    [
        ("PatientName", "DT", b"Doe^John", "PN"),
        ("PatientName", "DS", b"Doe^John", "PN"),
        ("FocalDistance", "DS", b"1\\5\x00", "IS"),
        # Undefined tag, no predefined VR in dictionary`
        (0x00100012, "DT", b"Doe^John", "UN"),
    ],
)
def test_apply_dictionary_VR(tag, VR, value, final_VR, evt):
    # Will remove DS from list, fail on IS, fallback sets VR to OB
    de = load_raw(tag, VR, len(value), value)
    assert de.VR == final_VR
    assert evt() == [f"Replace VR: {VR} -> {final_VR}"]


def test_apply_dictionary_VR_unknown_public_tag(evt):
    value = b"Doe^John"
    de = load_raw(0x00100012, "DT", len(value), value)
    assert de.VR == "UN"
    assert evt() == ["Replace VR: DT -> UN"]


@pytest.mark.parametrize(
    "validation_mode,value,VR,event",
    [
        # If validation_mode == 1, AS remains unchanged
        (1, "doe^john", "AS", []),
        # If validation_mode == 2, AS is fixed to be PN.
        (2, "doe^john", "PN", ["Replace VR: AS -> PN"]),
    ],
)
def test_converter_exception_value_error_fix_VR_mismatch_private_dict(
    evt, validation_mode, value, VR, event, config
):
    config.reading_validation_mode = validation_mode
    ds = Dataset()
    pc = load_raw(0x31030010, "LO", 27, b"AMI Sequence Annotations_01")
    ds[(0x3103, 0x0010)] = pc
    de = load_raw(0x31031080, "AS", 2, b"doe^john", dataset=ds)
    assert de.value == value
    if VR == "PN":
        assert isinstance(de.value, PersonName)
    assert de.VR == VR
    assert evt() == event


# TODO: Do we want to make these work?
@pytest.mark.xfail()
@pytest.mark.parametrize(
    "VR",
    [
        "IS",  # Fails because we first hit the truncate decimals fixer which raises
        "US",  # Fails because it takes char representation and casts to shorts.
    ],
)
def test_converter_exception_value_error_fix_VR_mismatch_private_dict_IS(evt, VR):
    ds = Dataset()
    pc = load_raw(0x31030010, "LO", 27, b"AMI Sequence Annotations_01")
    ds[(0x3103, 0x0010)] = pc
    de = load_raw(0x31031080, VR, 2, b"doe^john", dataset=ds)
    assert de.value == "Doe^John"
    assert de.VR == "PN"
    assert evt() == ["Replace VR: None -> LO", "Replace VR: None -> PN"]


def test_apply_dictionary_VR_correct_sequence_delimiter_VR(evt):
    de = load_raw(Tag(0xFFFEE0DD), "0x0000", 0, None)
    assert de.VR == "OB"
    assert evt() == ["Replace VR: 0x0000 -> OB"]


@pytest.mark.parametrize(
    "validation_mode,event,value",
    [
        # If validation_mode == 1, UN is changed to CS, but value remains as-is
        (1, ["Replace VR: UN -> CS"], "iso8859"),
        # If validation_mode == 2, CS VR needs to be all uppercase
        (
            2,
            ["Replace VR: UN -> CS", "Replace value: b'iso8859' -> b'ISO8859'"],
            "ISO8859",
        ),
    ],
)
def test_tag_specific_fixer_correct_specific_character_set(
    evt, validation_mode, event, value, config
):
    config.reading_validation_mode = validation_mode
    de = load_raw(Tag(0x00080005), "UN", 14, b"iso8859")
    assert de.VR == "CS"
    assert evt() == event
    assert de.value == value


def test_converter_exception_invalid_vr_for_pub_tag_value_raising_set_to_OB(ctx):
    # 0x00280010 is rows which has VR = US, and Wow cannot be set
    de = load_raw(Tag(0x00280010), "0x0000", 4, "Wow")
    assert de.VR == "US"
    assert de.value is None


def test_converter_exception_callback_invalid_vr_for_unk_tag_value_set_to_UN(ctx):
    de = load_raw(Tag(0x12340002), "0x0000", 4, "Wow")
    assert de.VR == "UN"


def test_LUT_descriptor(ctx, config):
    config.reading_validation_mode = 2
    config.remove_fixers("convert_exception_fixer")
    # Testing some fixes from pydicom
    # https://github.com/pydicom/pydicom/blob/935de3b4ac94a5f520f3c91b42220ff0f13bce54/pydicom/dataelem.py#L748
    value = (
        (-255).to_bytes(2, "little", signed=True)
        + (0).to_bytes(2, "little")
        + (124).to_bytes(2, "little", signed=True)
    )
    de = load_raw(Tag(0x00281101), "SS", 6, value)
    assert de.value == [32513, 0, 124]
    ds = Dataset()
    ctx.process_tracker({}, ds)
    assert len(ctx.modified_attrs) == 1
    elem = ctx.modified_attrs[(0x0028, 0x1101)]
    assert elem.value == ""
    assert len(ctx.nonconforming_elems) == 1
    nonconform_ds = ctx.nonconforming_elems[0]
    assert nonconform_ds.SelectorAttribute == (0x0028, 0x1101)
    assert nonconform_ds.NonconformingDataElementValue == value


def test_LUT_descriptor_VR_mismatch(ctx):
    # Testing some fixes from pydicom
    # https://github.com/pydicom/pydicom/blob/935de3b4ac94a5f520f3c91b42220ff0f13bce54/pydicom/dataelem.py#L748
    value = (
        (-255).to_bytes(2, "little", signed=True)
        + (0).to_bytes(2, "little")
        + (124).to_bytes(2, "little", signed=True)
    )
    de = load_raw(Tag(0x00281101), "OW", 6, value)
    # Unconverted
    assert de.value == value
    assert de.VR == "US or SS"
    ds = Dataset()
    ctx.process_tracker({}, ds)
    # VR only change, not added to modified attributes
    assert len(ctx.modified_attrs) == 0


def test_tracker_filter():
    with ReadContext() as ctx:
        _ = load_raw(0x00280010, None, 2, (1024).to_bytes(2, "little"))
    assert len(ctx.data_elements[0].events) == 1
    ctx.trim()
    assert len(ctx.data_elements) == 0


def test_private_nonconforming_not_added_to_modified(ctx):
    value = b"1.2\\1.3a"
    de = load_raw(Tag(0x00230223), "DS", len(value), value)
    assert de.VR == "DS"
    assert de.value == [1.2, 1.3]
    ds = Dataset()
    ctx.process_tracker({}, ds)
    assert len(ctx.nonconforming_elems) == 1
    assert ctx.nonconforming_elems[0].SelectorAttribute == 0x00230223


@pytest.mark.parametrize(
    "validation_mode, value1, nonconforming, value2",
    [
        # If validation_mode == 1, inputted values remain unchanged.
        (1, "Hello World, itsa me", 0, "Hello World"),
        # If validation_mode == 2, both values are changed.
        (2, "", 1, "HELLO WORLD"),
    ],
)
def test_crop_text_vrs_off(
    ctx, evt, validation_mode, value1, nonconforming, value2, config
):
    config.reading_validation_mode = validation_mode
    val = b"Hello World, itsa me"
    de = load_raw((0x0010, 0x0040), "CS", len(val), val)
    assert de.value == value1
    # First event from converter_exception validation since all CS characters
    # need to be uppercase.  Second because validation fails, too long for CS,
    # converts value to OB to pass validation (no validation on OB)
    ds = MagicMock()
    ctx.process_tracker({0x00010001}, ds)
    assert len(ctx.nonconforming_elems) == nonconforming
    if nonconforming > 0:
        elem = ctx.nonconforming_elems[0]
        assert elem.SelectorAttribute == (0x0010, 0x0040)
        assert elem.SelectorValueNumber == 0
        assert elem.NonconformingDataElementValue == val
        evts = evt()
        assert evts == [
            "Replace value: b'Hello World, itsa me' -> ",
            "Replace length: 20 -> 0",
        ]

    de = load_raw((0x0010, 0x0040), "CS", 11, b"Hello World")
    assert de.value == value2
    if value2 == "HELLO WORLD":
        assert evt() == [
            *evts,  # Left over from previous
            "Replace value: b'Hello World' -> b'HELLO WORLD'",
        ]


@pytest.mark.parametrize(
    "validation_mode,final_value,VR",
    [(2, b"Hello World, itsa me", "OB"), (1, "Hello World, itsa me", "CS")],
)
def test_crop_text_vrs_read_only(ctx, evt, config, validation_mode, final_value, VR):
    config.read_only = True
    config.reading_validation_mode = validation_mode
    val = b"Hello World, itsa me"
    de = load_raw((0x0010, 0x0040), "CS", len(val), val)
    # Without strict validation, VR remainds unchanged
    # as it's not checking for adherance to CS DICOM standard.
    # Value is converted from bytes.
    # With strict validation and in read_only mode,
    # value should remain unchanged to preserve the maximum
    # information available. Value remainds as bytes.
    # VR changes from CS to OB as the data does not fit
    # DICOM standard for CS.
    assert de.value == final_value
    assert de.VR == VR


@pytest.mark.parametrize(
    "validation_mode,value,event",
    [
        # If validation_mode == 1, the value remains the same through the earlier fixers and then is only truncated.
        (
            1,
            "Hello World itsa",
            [
                "Replace value: b'Hello World itsa me' -> b'Hello World itsa'",
                "Replace length: 19 -> 16",
            ],
        ),
        # If validation_mode == 2, converter_exception is called first, fails validation, value removed
        (
            2,
            "",
            ["Replace value: b'Hello World itsa me' -> ", "Replace length: 19 -> 0"],
        ),
    ],
)
def test_crop_text_vrs_after_convert_exception(
    evt, config, validation_mode, value, event
):
    config.reading_validation_mode = validation_mode
    config.add_fixers("fw_file.dicom.fixers.crop_text_VR", "value", 4)
    de = load_raw((0x0010, 0x0040), "CS", 19, b"Hello World itsa me")
    assert de.value == value
    assert evt() == event


@pytest.mark.parametrize(
    "validation_mode, value, event",
    [
        # If validation_mode == 1, the value is truncated but unchanged by the caps fixer
        (1, "Hello World itsa", []),
        # If validation_mode == 2, the value is both truncated and fixed to be UPPER
        (
            2,
            "HELLO WORLD ITSA",
            ["Replace value: b'Hello World itsa' -> b'HELLO WORLD ITSA'"],
        ),
    ],
)
def test_crop_text_vrs_on(evt, config, validation_mode, value, event):
    config.reading_validation_mode = validation_mode
    config.add_fixers("fw_file.dicom.fixers.crop_text_VR", "value", 1)
    de = load_raw((0x0010, 0x0040), "CS", 19, b"Hello World itsa me")
    assert de.value == value
    shared_events = [
        "Replace value: b'Hello World itsa me' -> b'Hello World itsa'",
        "Replace length: 19 -> 16",
    ]
    assert evt() == shared_events + event


@pytest.mark.parametrize(
    "validation_mode,value,event",
    [
        # If validation_mode == 1, no fixers are applied
        (1, "Hello World", []),
        # If validation_mode == 2, value is changed to UPPER
        (2, "HELLO WORLD", ["Replace value: b'Hello World' -> b'HELLO WORLD'"]),
    ],
)
def test_CS_VR_capitalized(evt, validation_mode, value, event, config):
    config.reading_validation_mode = validation_mode
    de = load_raw((0x0010, 0x0040), "CS", 11, b"Hello World")
    assert de.value == value
    assert evt() == event


def test_load_pydicom_dataset_with_invalid_VR_inside_sequence(dicom_file, evt):
    # Simply test we can read this file.  As of pydicom 2.2, it is no longer
    # possible to reproduce the original NotImplementedError that was raised
    # when trying to read this file.
    path = dicom_file("invalid", "invalid_VR_in_UN_sequence.dcm")
    dcm = dcmread(path, force=True)
    de1 = dcm[0x00189346][0][0x00080100]
    assert de1.VR == "SH"
    assert de1.value == "113691"
    de2 = dcm[0x00189346][0][0x00080104]
    assert de2.VR == "LO"
    assert de2.value == "IEC Body Dosimetry Phantom"
    assert evt() == [
        "Replace VR: UN -> SQ",
        "Replace VR: None -> SH",
        "Replace VR: None -> LO",
    ]


def test_context_manager_sets_UN_VRs(dicom_file, evt):
    path = dicom_file("invalid", "invalid_VR_in_UN_sequence_UN_PatientID.dcm")
    dcm = dcmread(path, force=True)
    dcm.decode()
    assert dcm["PatientID"].VR == "LO"
    seq = dcm["CTDIPhantomTypeCodeSequence"]
    assert seq.VR == "SQ"
    assert seq[0][0x00080102].VR == "SH"
    assert seq[0][0x00080102].value == "DCM"
    assert evt() == [
        "Replace VR: UN -> LO",
        "Replace VR: UN -> SQ",
        "Replace VR: None -> SH",
        "Replace VR: None -> SH",
        "Replace VR: None -> LO",
    ]


def test_load_pydicom_dataset_with_invalid_public_tag(dicom_file, evt):
    path = dicom_file("invalid", "explicitTS_invalid_VR_public_tag.dcm")
    dcm = dcmread(path, force=True)
    de = dcm[0x000C0010]
    # Accessing the invalid public tag above would previously raised on pydicom < 2.1.0
    # asserting this not the case.
    assert de.VR == "UN"
    assert de.value == b"whatever"
    assert evt() == ["Replace VR: None -> UN"]


@pytest.mark.parametrize(
    "path",
    [
        f
        for f in data.get_testdata_files()
        if f.endswith(".dcm") and not f.endswith("no_meta.dcm")
    ],
)
def test_can_decode_all_pydicom_test_files(path):
    dcm = DICOM(path, force=True)
    dcm.decode()


@pytest.mark.parametrize(
    "validation_mode,event",
    [
        # Output should be the same regardless of validation_mode
        (
            1,
            [
                "Replace VR: None -> IS",
            ],
        ),
        (
            2,
            [
                "Replace VR: None -> IS",
                # Length is 4 because of null terminator
                "Replace length: 4 -> 2",
                "Replace value: b'12.5' -> b'12'",
            ],
        ),
    ],
)
def test_None_VR_and_truncate_IS_float(dicom_file, evt, validation_mode, event, config):
    config.reading_validation_mode = validation_mode
    path = dicom_file("invalid", "invalid_float_in_is_string_implicit.dcm")
    dcm = dcmread(path, force=True)
    dcm.decode()
    dcm.decode()
    assert evt() == event
