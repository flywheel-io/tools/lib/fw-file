"""Tests for png module."""

import io
from datetime import datetime

import png as pypng
import pytest

from fw_file.exif import EXIF
from fw_file.png import PNG, Chunk, iTXt, tEXt, zTXt


@pytest.fixture
def png():
    file = io.BytesIO()
    w = pypng.Writer(16, 1)
    w.write(file, [range(16)])
    file.seek(0)
    return PNG(file)


def test_png_default_meta(png, tmp_path):
    assert png.get_meta() == {"file.type": "image"}
    path = tmp_path / "test.png"
    with path.open("wb") as fp:
        png.save(fp)
    png = PNG(path)
    assert png.get_meta() == {
        "file.name": "test.png",
        "file.type": "image",
    }


def test_png_save(png, tmp_path):
    path = tmp_path / "test.png"
    png.save(path)
    assert path.stat().st_size > 0


def test_png_save_open_file(png, tmp_path):
    path = tmp_path / "test.png"
    with path.open("wb") as fp:
        png.save(fp)
    assert path.stat().st_size > 0


def test_png_get(png):
    assert png.img.width == 16
    assert png.get("unknown") is None


def test_png_set(png, tmp_path):
    png.title = "Test PNG"
    png.author = tEXt("Author", "Test Author")
    png.description = zTXt("Description", "Test description")
    png.other = iTXt("Other", "Test compressed", zip_=True)
    png.save(tmp_path / "test.png")
    png = PNG(tmp_path / "test.png")
    assert isinstance(png.title, iTXt)
    assert png.title == "Test PNG"
    assert isinstance(png.author, tEXt)
    assert png.author == "Test Author"
    assert isinstance(png.description, zTXt)
    assert png.description == "Test description"
    assert isinstance(png.other, iTXt)
    assert png.other == "Test compressed"
    assert png.other.zip


def test_set_ihdr_directly_raises(png):
    with pytest.raises(KeyError, match="cannot be modified directly"):
        png.ihdr = b"\n"


def test_png_set_bytes_chunk(png, tmp_path):
    png.bkgd = b"\0"
    assert isinstance(png.bkgd, Chunk)
    assert png.bkgd.type == "bKGD"
    png.save(tmp_path / "test.png")
    png = PNG(tmp_path / "test.png")
    assert png.bkgd == b"\0"


def test_png_set_raises_on_unexpected_value(png):
    with pytest.raises(ValueError, match="Unexpected value"):
        png.bkgd = object()


def test_png_timestamp(png, tmp_path):
    png.time = b'\x07\xe3\x01\x07\n\r"'
    assert png.get("time") == datetime(2019, 1, 7, 10, 13, 34)
    png.save(tmp_path / "test.png")
    png = PNG(tmp_path / "test.png")
    assert png.get("time") == datetime(2019, 1, 7, 10, 13, 34)


def test_png_del(png, tmp_path):
    png.title = "Test PNG"
    png.save(tmp_path / "test.png")
    png = PNG(f"{tmp_path}/test.png")
    assert png.title == "Test PNG"
    del png.title
    png.save(tmp_path / "test.png")
    png = PNG(f"{tmp_path}/test.png")
    assert png.get("title") is None


def test_png_multi_chunk_type(png, tmp_path):
    path = tmp_path / "test.png"
    png.splt = Chunk("sPLT", b"\0\0\0\0")
    png.splt_1 = Chunk("sPLT", b"\0\0\0\1")
    png.save(path)
    png = PNG(path)
    assert png.splt == b"\0\0\0\0"
    assert png.splt_1 == b"\0\0\0\1"


def test_png_set_exif(png, tmp_path):
    png.exif = EXIF()
    png.exif.ISOSpeed = 2
    png.save(tmp_path / "test.png")
    png = PNG(tmp_path / "test.png")
    assert png.get("exif").isospeed == 2
