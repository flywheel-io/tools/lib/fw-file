from datetime import datetime, timezone

import mne
import pytest

from fw_file.eeg import BDF, EDF, EEGLAB, BrainVision, MneEeg

from .conftest import ASSETS_ROOT

EEG_ROOT = ASSETS_ROOT / "eeg"

FMT_TO_EXT = {"edf": ".edf", "bdf": ".bdf", "eeglab": ".set", "brainvision": ".vhdr"}


@pytest.fixture(
    params=[
        [BrainVision, "Analyzer_nV_Export.vhdr"],
        [EDF, "chtypes_edf.edf"],
        [BDF, "test_bdf_stim_channel.bdf"],
        [EEGLAB, "test_raw_onefile.set"],
        [EEGLAB, "test_raw.set"],
        [MneEeg, "Analyzer_nV_Export.vhdr"],
    ]
)
def setup_all(request):
    # returns (Class, Filepath) for all EEG types and the base MNE class
    return request.param[0], (EEG_ROOT / request.param[1])


@pytest.fixture(
    params=[
        [BrainVision, "Analyzer_nV_Export.vhdr"],
        [EDF, "chtypes_edf.edf"],
        [EEGLAB, "test_raw_onefile.set"],
        [EEGLAB, "test_raw.set"],
        [MneEeg, "Analyzer_nV_Export.vhdr"],
    ]
)
def setup_non_bdf(request):
    # returns (Class, Filepath) for all EEG types except for BDF
    # this is because BDF is read-only
    return request.param[0], (EEG_ROOT / request.param[1])


@pytest.fixture(
    params=[
        [BrainVision, "Analyzer_nV_Export.vhdr"],
        [EDF, "chtypes_edf.edf"],
        [MneEeg, "Analyzer_nV_Export.vhdr"],
    ]
)
def setup_edf_bv_base(request):
    # returns (Class, Filepath) for BrainVision, EDF and base class
    # these classes share tests of the save method
    return request.param[0], (EEG_ROOT / request.param[1])


@pytest.fixture(params=[[BDF, "test_bdf_stim_channel.bdf"]])
def setup_bdf(request):
    # returns (Class, Filepath) for BDF type (read-only)
    return request.param[0], (EEG_ROOT / request.param[1])


@pytest.fixture(params=[[EEGLAB, "test_raw_onefile.set"], [EEGLAB, "test_raw.set"]])
def setup_eeglab(request):
    # returns (Class, Filepath) of EEGLAB types
    # EEGLAB class requires unique test of save method
    return request.param[0], (EEG_ROOT / request.param[1])


@pytest.fixture(
    params=[
        [BrainVision, "brainvision.zip", "Analyzer_nV_Export.vhdr"],
        [EEGLAB, "eeglab.zip", "test_raw.set"],
    ]
)
def setup_zipped(request):
    # returns (Class, Archive Filepath, Filepath) of types that parse zipped inputs
    return (
        request.param[0],
        (EEG_ROOT / request.param[1]),
        (EEG_ROOT / request.param[2]),
    )


def test_init(setup_all):
    EEG, filepath = setup_all
    eeg = EEG(filepath)
    assert eeg.nchan > 0  #'nchan' is mandatory attribute with value >0


def test_save_new_file(setup_edf_bv_base, tmp_path):
    EEG, filepath = setup_edf_bv_base
    eeg = EEG(filepath)
    ext = FMT_TO_EXT[eeg.file_format]
    epoch_ts = datetime(1985, 1, 1, 0, 0, 0, 0, tzinfo=timezone.utc)
    eeg.fields.set_meas_date(epoch_ts)
    eeg.save((tmp_path / "testfile").with_suffix(ext), overwrite=True)
    eeg2 = EEG((tmp_path / "testfile").with_suffix(ext))
    assert eeg2.meas_date == epoch_ts


def test_save_new_file_eeglab(setup_eeglab, tmp_path):
    EEG, filepath = setup_eeglab
    eeg = EEG(filepath)
    ext = FMT_TO_EXT[eeg.file_format]
    mne.rename_channels(eeg.fields, {eeg.ch_names[0]: "EEG 999"})
    eeg.save((tmp_path / "testfile").with_suffix(ext), overwrite=True)
    eeg2 = EEG((tmp_path / "testfile").with_suffix(ext))
    assert "EEG 999" in eeg2.ch_names


def test_save_new_file_bdf(setup_bdf):
    EEG, filepath = setup_bdf
    eeg = EEG(filepath)
    with pytest.raises(TypeError):
        eeg.save(overwrite=True)


def test_save_same_file(setup_edf_bv_base, tmp_path):
    EEG, filepath = setup_edf_bv_base
    tmp_eeg = EEG(filepath)
    ext = FMT_TO_EXT[tmp_eeg.file_format]
    tmp_eeg.save((tmp_path / "testfile").with_suffix(ext), overwrite=True)
    eeg = EEG((tmp_path / "testfile").with_suffix(ext))
    epoch_ts = datetime(1985, 1, 1, 0, 0, 0, 0, tzinfo=timezone.utc)
    eeg.fields.set_meas_date(epoch_ts)
    eeg.save(overwrite=True)
    eeg2 = EEG((tmp_path / "testfile").with_suffix(ext))
    assert eeg2.meas_date == epoch_ts


def test_save_excludes_unsupported_metadata(setup_non_bdf, tmp_path):
    """Unsupported metadata (subject_info.id) should not be saved"""

    EEG, filepath = setup_non_bdf
    eeg = EEG(filepath)
    ext = FMT_TO_EXT[eeg.file_format]
    eeg.subject_info = {"id": 1}
    assert eeg.subject_info
    eeg.save((tmp_path / "testfile").with_suffix(ext), overwrite=True)
    eeg2 = EEG((tmp_path / "testfile").with_suffix(ext))
    # following asserts both check same condition for different filetypes
    if eeg2.subject_info:
        assert "id" not in eeg2.subject_info.keys()
    else:
        assert not eeg2.subject_info


def test_save_raises_invalid_file(setup_edf_bv_base, tmp_path):
    """Saving file without required metadata raises an error"""
    EEG, filepath = setup_edf_bv_base
    eeg = EEG(filepath)
    ext = FMT_TO_EXT[eeg.file_format]
    del eeg.sfreq
    with pytest.raises(KeyError):
        eeg.save((tmp_path / "testfile").with_suffix(ext), overwrite=True)


def test_len(setup_all):
    EEG, filepath = setup_all
    eeg = EEG(filepath)
    assert len(eeg) == len(eeg.fields)


def test_getattr(setup_all):
    EEG, filepath = setup_all
    eeg = EEG(filepath)
    assert eeg.nchan > 0


def test_getattr_empty(setup_all):
    EEG, filepath = setup_all
    eeg = EEG(filepath)
    assert not eeg.experimenter


def test_getattr_error(setup_all):
    EEG, filepath = setup_all
    eeg = EEG(filepath)
    with pytest.raises(AttributeError):
        eeg.invalid_attr


def test_setattr(setup_non_bdf):
    EEG, filepath = setup_non_bdf
    eeg = EEG(filepath)
    bad_ch = eeg.ch_names[0]
    eeg.bads = [bad_ch]
    assert eeg.bads == [bad_ch]


def test_setattr_readonly_attr(setup_bdf):
    EEG, filepath = setup_bdf
    eeg = EEG(filepath)
    with pytest.raises(TypeError):
        eeg.sfreq = 10


def test_delattr(setup_non_bdf):
    EEG, filepath = setup_non_bdf
    eeg = EEG(filepath)
    assert eeg.sfreq
    delattr(eeg, "sfreq")
    with pytest.raises(AttributeError):
        eeg.sfreq


def test_delattr_readonly_attr(setup_bdf):
    EEG, filepath = setup_bdf
    eeg = EEG(filepath)
    assert eeg.sfreq
    with pytest.raises(TypeError):
        delattr(eeg, "sfreq")


def test_getmeta(setup_all):
    EEG, filepath = setup_all
    eeg = EEG(filepath)
    assert eeg.get_meta()


def test_raw(setup_all):
    EEG, filepath = setup_all
    eeg = EEG(filepath)
    assert eeg.raw


def test_baseclass_bdf_raises():
    with pytest.raises(RuntimeError):
        MneEeg(EEG_ROOT / "test_bdf_stim_channel.bdf")


def test_brainvision_zip(setup_zipped):
    EEG, archive, filepath = setup_zipped
    eeg_from_zip = EEG.from_zip(archive)
    eeg_from_file = EEG(filepath)
    assert eeg_from_zip.get_meta() == eeg_from_file.get_meta()
