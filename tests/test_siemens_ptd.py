"""Tests for PTD module."""

import io
import struct

import pytest
from fw_utils import BinFile

from fw_file.dicom.testing import DCM_DEFAULT, create_dcm
from fw_file.siemens import PTD_MAGIC_STR, PTDFile


def create_ptd(file=None, preamble=b"", **dcmdict):
    file = file or io.BytesIO()
    dcm = io.BytesIO()
    create_dcm(**dcmdict).save(dcm)
    dcm_bytes = dcm.getvalue()
    with BinFile(file, mode="wb") as wfile:
        wfile.write(preamble)
        wfile.write(dcm_bytes)
        wfile.write(struct.pack("i", len(dcm_bytes)))
        wfile.write(PTD_MAGIC_STR)
    if isinstance(file, io.BytesIO):
        file.seek(0)
    return PTDFile(file)


def test_init_raises_on_invalid_magic_str():
    file = io.BytesIO()
    file.write(b"invalid-ptd-magic-string")
    file.seek(0)
    with pytest.raises(ValueError):
        PTDFile(file)


def test_init_raises_on_invalid_dicom():
    file = io.BytesIO()
    invalid_dcm = b"invalid-dcm"
    file.write(invalid_dcm)
    file.write(struct.pack("i", len(invalid_dcm)))
    file.write(PTD_MAGIC_STR)
    file.seek(0)
    with pytest.raises(ValueError):
        PTDFile(file)


def test_getattr():
    ptd = create_ptd(PatientID="test")
    assert ptd.PatientID == "test"


def test_setattr():
    ptd = create_ptd(PatientID="test")
    ptd.PatientID = "foo"
    assert ptd.PatientID == "foo"


def test_delattr():
    ptd = create_ptd(PatientID="test")
    del ptd.PatientID
    assert "PatientID" not in ptd


def test_iter():
    ptd = create_ptd()
    assert len(list(iter(ptd))) == len(DCM_DEFAULT)


def test_len():
    ptd = create_ptd()
    assert len(ptd) == len(DCM_DEFAULT)


def test_meta():
    ptd = create_ptd()
    assert ptd.get_meta() == {
        "subject.label": "test",
        "session.uid": "1",
        "session.label": "1",
        "acquisition.uid": "1.2",
        "acquisition.label": "1.2",
        "file.name": "1.2.3.MR.ptd",  # meta-based in lack of path
        "file.type": "ptd",
    }


def test_save(tmp_path):
    ptd = create_ptd(preamble=b"test", PatientID="test")
    file = tmp_path / "test.ptd"
    ptd.save(file)

    content = file.read_bytes()
    assert content.startswith(b"test")
    assert content.endswith(PTD_MAGIC_STR)
    assert PTDFile(file).PatientID == "test"
