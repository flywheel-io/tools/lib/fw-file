import pytest
from pydicom import data, dcmread
from pydicom.datadict import keyword_for_tag
from pydicom.tag import BaseTag

from fw_file.dicom import DICOM
from fw_file.dicom.testing import create_dcm
from fw_file.dicom.validation import (
    Standard,
    get_standard,
    get_tag_dict,
    validate_dicom,
)

testdata_files = [
    file
    for file in data.get_testdata_files()
    if file.endswith(".dcm") and not file.endswith("no_meta.dcm")
]


@pytest.mark.parametrize("rev", ["current", "2023c"])
def test_get_standard(config, rev, tmp_path):
    config.standard_rev = rev
    if rev == "current":
        # Make sure it's saving to tmp
        config.standard_path = tmp_path / "dicom_standard"
    res = get_standard()
    assert isinstance(res, Standard)
    assert isinstance(res.dictionary, dict)
    assert isinstance(res.modules, dict)
    assert isinstance(res.iods, dict)


@pytest.mark.parametrize(
    "sop,exp",
    [
        # MR Image Storage
        (
            "1.2.840.10008.5.1.4.1.1.4",
            # MR specific tags
            ["SequenceVariant", "ScanningSequence"],
        ),
        # Secondary Capture Image Storage
        ("1.2.840.10008.5.1.4.1.1.7", ["ConversionType"]),
        # Ophthalmic Tomography Image Storage
        ("1.2.840.10008.5.1.4.1.1.77.1.5.4", ["ImageLaterality"]),
        # RT StructureSet
        (
            "1.2.840.10008.5.1.4.1.1.481.3",
            ["ROIContourSequence", "RTROIObservationsSequence", "StructureSetLabel"],
        ),
    ],
)
def test_get_tag_dict_no_conditionals(sop, exp):
    standard = get_standard()
    dcm = create_dcm(SOPClassUID=sop)
    tags = get_tag_dict(standard, dcm)
    req = {keyword_for_tag(tag) for tag, v in tags.items() if not v}
    assert all(tag in req for tag in exp)


def test_get_tag_dict_with_conditional():
    standard = get_standard()
    dcm = create_dcm(
        SOPClassUID="1.2.840.10008.5.1.4.1.1.4",
        # Opt into optional Clinical Trial Subject Module
        # PS3.3 C.7.1.3
        ClinicalTrialSponsorName="test",
        ClinicalTrialSubjectID="test",
    )
    tags = get_tag_dict(standard, dcm)
    req = {keyword_for_tag(tag) for tag, v in tags.items() if not v}
    assert "ClinicalTrialSponsorName" in req
    assert "ClinicalTrialProtocolID" in req
    assert "ClinicalTrialSubjectID" in req
    assert "ClinicalTrialSubjectReadingID" not in req


def test_get_tag_dict_with_AT():
    # This test looks at the behavior around DICOM tags
    # like Frame Increment Pointer when interacting with dicom-validator.
    # Frame Increment Pointer creates the following condition:
    # condition = {'index': 0, 'op': '=', 'tag': '(0028,0009)', 'type': 'MN',
    # 'values': ['Frame Time (0018,1063)', 'Frame Time Vector (0018,1065)']}
    # dicom-validator:0.3.5 attempted to parse the list of values as
    # ints to feed into BaseTag and raises a ValueError, but this seems
    # to be fixed in later versions.
    standard = get_standard()
    dcm = create_dcm(
        SOPClassUID="1.2.840.10008.5.1.4.1.1.7.4",
        # Multi-frame True Color Secondary Capture Image Storage
        FrameIncrementPointer=BaseTag(0x00280009),
        FrameTime=0,
    )
    tags = get_tag_dict(standard, dcm)
    assert tags[1577059] is True


@pytest.mark.parametrize("path", testdata_files)
def test_can_validate_all_pydicom_test_files(path):
    dcm = DICOM(path, force=True)
    dcm.decode()
    standard = get_standard()
    _ = validate_dicom(standard, dcm)


def test_can_validate_from_pydicom_dataset():
    file_ = testdata_files[0]
    dcm = dcmread(file_, force=True)
    standard = get_standard()
    _ = validate_dicom(standard, dcm)
